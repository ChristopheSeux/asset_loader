from .Property import Property
from .Picture import Picture
from ..functions import *

from time import time

class Image(Property):

    background_color = (0.4, 0.4, 0.4, 0.85)
    border_width = 1
    border_style = 'solid'
    border_color = (0.15, 0.15, 0.15, 1)
    border_color_selected = (0.85, 0.85, 0.85, 1)

    def __init__(self,image,text=None,size = None,frame_method = 'Stretch'):


        self.picture = Picture(image,size = size)

        self.size = size
        if not size :
            self.size = self.picture.size

        self.width = self.size[0]
        self.height = self.size[1]
            #print(self.size)

        self.select = False

        self.setText(text)


    def setMinimumSize(self,size = None) :
        #return
        self.minimum_width = self.size[0]
        """
        if size is None :
            if self.icon and not self.text :
                self.minimum_width =  22
                return
            dim = 2*self.margin[0]
            text_dim = self.getTextSize(self.text)
            dim+= text_dim[0]

            if self.icon :
                dim += 10

            self.minimum_width =  dim
        else :
            self.minimum_width =  size
            """

    def save_image(self, path) :
        import PIL
        import numpy as np

        pic = self.picture

        pixels = np.flipud(pic.data) #flip up down
        pixels = (pixels*255).astype(np.uint8) # set between 0->255
        pixels = pixels.reshape(pic.img_size[0],pic.img_size[1],4)

        im = PIL.Image.fromarray(pixels,'RGBA')
        im = im.resize(size= (140,140), resample=PIL.Image.BILINEAR)
        im.save(path)

    def processEvent(self) :
        self.isMouseOver()
        self.boundBox()

        #print(self.left_mouse.double_click)

        if self.is_over  :
            #print(self.left_mouse.press_count)

            if self.left_mouse.double_click :
                print('double_click')
                #print(time() - self.left_mouse.press_time)
                #print(self)
                #print('double_click')
                self.select = True
                self.run_action()
                #self.left_mouse.press_time = 0

            if self.left_mouse.press and not self.is_pressed :
                self.is_pressed = True


            if self.left_mouse.press or self.right_mouse.press:
                self.select = True


        #print(self.left_mouse.release)

        else :
            if self.left_mouse.release or self.right_mouse.release and not self.is_pressed :
                self.select = False

        if self.event_value =='RELEASE':
            self.is_pressed = False

    def draw(self) :
        glEnable(GL_BLEND)

        self.processEvent()

        box = self.bound_box
        #glBlendFunc(GL_ONE,GL_ONE)
        #print(self.select)

        #background default color
        bg_color = self.background_color

        #Background mouse hover
        #if self.is_pressed :
        #    bg_color = self.changeColor(bg_color,self.dark_offset*2)

        if self.select :
            bg_color = self.changeColor(bg_color,self.bright_offset)

        if self.is_over :
            bg_color = self.changeColor(bg_color,self.bright_offset)


        draw_square(*box,bg_color,primitive = GL_POLYGON)

        border_color = self.border_color
        if self.select:
            border_color = self.border_color_selected


        #Draw Image
        self.picture.pos = box[3]
        self.picture.draw()


        blf.enable(self.font_id, blf.SHADOW)

        blf.size(self.font_id, 13, 72)

        if self.text :
            display_text = self.text.replace('_',' ')
            display_text = display_text[0].upper() + display_text[1:]

            dim = self.getTextSize(display_text)

            txt_size = 13
            if dim[0] > self.size[0] :
                #factor = self.size[0] / dim
                txt_size = round(13 * self.size[0] / (dim[0]))

                blf.size(self.font_id, txt_size, 72)

            dim = self.getTextSize(display_text)

            center = box[3]  + Vector((self.width/2,0))

            tex_pos = center + Vector((-dim[0]/2,-20+self.margin[1]))
            #draw txt bg

            p1 = box[3]
            p2 = box[2]
            p3 = box[2] - Vector((0,20))
            p4 = box[3] - Vector((0,20))

            draw_square(p1,p2,p3,p4,color = (0.3,0.3,0.3,1))


            glColor3f(0.95,0.95,0.95)


            #tex_pos += Vector((0,self.margin[1]))
            #tex_pos -= Vector((self.getTextSize(self.text)[0]/2,0))
            #tex_pos = box[2]
            # draw op name
            blf.position(self.font_id, *tex_pos, 0)
            #blf.size(self.font_id, txt_size, 72)
            blf.shadow(self.font_id , 3 ,0.0 ,0.0 ,0.0 ,1)

            blf.draw(self.font_id, display_text)

        blf.disable(self.font_id, blf.SHADOW)

        #contour
        if self.border_width and self.border_style not in ('none','hidden'):
            if self.border_style == 'dashed' : glEnable(GL_LINE_STIPPLE)

            glLineWidth(self.border_width)
            if self.text :
                contour_box = [box[0],box[1],box[2]-Vector((0,20)),box[3]-Vector((0,20))]
            else :
                contour_box = box

            draw_square(*contour_box,border_color,primitive = GL_LINE_STRIP)

        # restore opengl defaults
        glDisable(GL_LINE_STIPPLE)
        glLineWidth(1)
        glDisable(GL_BLEND)
        glColor4f(0.0, 0.0, 0.0, 1.0)
