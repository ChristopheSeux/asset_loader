from .functions import *
from bpy.props import *

def folder_items(self,context) :
    return [(f,f,"") for f in self['asset_folders']]

class AssetLoaderSettings(bpy.types.PropertyGroup) :
    #folders = EnumProperty(items = folder_items)
    search = StringProperty()
    make_proxy = BoolProperty(default =True)

    pass
    #images = bpy.utils.previews.new()
    #pass

    #icons = bpy.utils.previews.new()






class AssetLoaderPrefs(bpy.types.AddonPreferences):
    bl_idname = __package__

    lib_dir = StringProperty(
        name="Asset Lib Folder",
        subtype = 'FILE_PATH',
        default = 'lib',
        description = 'Dir folder relative to the store')

    store = StringProperty(name="Store",subtype = 'FILE_PATH')



    def draw(self, context):
        layout = self.layout
        layout.prop(self, "lib_dir")

        if not env_vars.get('STORE') :
            layout.prop(self, "store")
