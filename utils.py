import bpy
import blf
from bgl import *
from mathutils import Vector,Color
import numpy as np
from os.path import join,basename,dirname,splitext,abspath
#from PIL import Image
import time
from os import environ as env_vars
from os import listdir,mkdir,chdir,scandir
from os.path import join,dirname,splitext,isdir,exists,basename,abspath
import json



batch_folder = join(dirname(__file__),"batch")

def library_abspath(filepath) :
    return abspath(bpy.path.abspath(filepath))




def mirror_key(prop,index,attributes,bezier) :
    from copy import deepcopy

    mirror_attributes= deepcopy(attributes)
    mirror =False

    if prop in ['rotation_euler']:
        if index in [1,2] :
            mirror = True

    elif prop in ['rotation_quaternion','rotation_axis_angle']:
        if index in [2,3] :
            mirror = True

    elif prop in['location'] :
        if index in [0] :
            mirror = True

    if mirror :
        mirror_attributes[1][1] = -attributes[1][1] #value

        if bezier :
            mirror_attributes[3][1] = -attributes[1][1]+(-attributes[1][1]-attributes[3][1])#handle_left y
            mirror_attributes[5][1] = -attributes[1][1]+(-attributes[1][1]-attributes[5][1])#handle_right y

    return mirror_attributes


def find_mirror(name) :
    mirror = None
    prop = False

    if name :

        if name.startswith('[')and name.endswith(']'):
            prop = True
            name= name[:-2][2:]

        match={
        'R' : 'L',
        'r' : 'l',
        'L' : 'R',
        'l' : 'r',
        }

        separator=['.','_']

        if name.startswith(tuple(match.keys())):
            if name[1] in separator :
                mirror = match[name[0]]+name[1:]

        if name.endswith(tuple(match.keys())):
            if name[-2] in separator :
                mirror = name[:-1]+match[name[-1]]

        if mirror and prop == True:
            mirror='["%s"]'%mirror

        return mirror

    else :
        return None

def get_name_side(name) :
    mirror_name = find_mirror(name)
    if not mirror_name :
        return

    side_letter = [i for i,j in zip(name,mirror_name) if i!=j][0]

    #print(name,side_letter)

    if side_letter in ('L','l') :
        return 'LEFT'

    elif side_letter in ('R','r') :
        return 'RIGHT'




def scantree(path):
    """Recursively yield DirEntry objects for given directory."""
    for entry in scandir(path):
        if entry.is_dir(follow_symlinks=False):
            yield from scantree(entry.path)  # see below for Python 2.x
        else:
            yield entry

def get_action(ob) :
    anim_data = ob.animation_data
    if not anim_data : return

    return anim_data.action

def load_data_block(path,asset_type,asset,link=True) :
    with bpy.data.libraries.load(path, link=link) as (data_src, data_dst):
        #all_groups = [name for name in data_src.groups]
        setattr(data_dst,asset_type,[asset])

    if not data_dst :
        print('The datablock %s is not found'%asset)
        return

    return getattr(data_dst,asset_type)[0]

def icon_path(icon_name):
    icon_folder = join(dirname(__file__),'icons')

    return join(icon_folder, icon_name+'.png')

def bl_icon_path(icon_name):
    icon_folder = join(dirname(__file__),'icons','blender')

    return join(icon_folder, icon_name+'.png')


def read_json(filepath) :
    if exists(filepath):
        try :
            with open(filepath) as data_file:
                info = json.load(data_file)
                return info
        except  :
            print('the json file : %s is corrupted' %filepath)

def write_json(filepath,data) :
    #f = open(filepath,'w')
    #f.write(json.dumps(data,indent='\t',sort_keys=True))
    #f.close()
    with open(filepath,"w") as out_file:
        json.dump(data,out_file)


def get_store():
    bl_prefs = bpy.context.user_preferences
    addon_prefs = bl_prefs.addons[__package__].preferences

    return env_vars.get('STORE',addon_prefs.store)

def get_lib_dir():
    bl_prefs = bpy.context.user_preferences
    addon_prefs = bl_prefs.addons[__package__].preferences

    store = get_store()

    if not store :
        print("No store in the prefs")
        return

    if not addon_prefs.lib_dir :
        print("No Lib directory in the prefs")
        return

    lib_dir = join(store, addon_prefs.lib_dir)
    if not exists(lib_dir) :
        print("Lib directory not exist")
        return

    return lib_dir


def get_poselib_dir() :
    ob = bpy.context.object
    library = ob.data.library
    rig_dir = dirname(library_abspath(library.filepath))
    poseLib_path = join(rig_dir, 'poseLib')

    return poseLib_path


def get_group(ob,group_name) :
    if isinstance(ob,bpy.types.PoseBone) :
        ob = ob.id_data

    group = ob.animation_data.action.groups.get(group_name)

    if not group :
        group = ob.animation_data.action.groups.new(group_name)

    return group

def get_fcurve(ob,data_path,index=-1) :
    import re
    anim_data = ob.animation_data

    if not anim_data :
        ob.animation_data_create()

    if not anim_data.action :
        name = re.sub(r'[\._-]+rig$', '_proxy_action', ob.name, flags=re.I)
        action = bpy.data.actions.new(name)
        anim_data.action = action

    fc = anim_data.action.fcurves.find(data_path,index=index)
    if not fc :
        fc = anim_data.action.fcurves.new(data_path,index)

    return fc

def add_keyframe(ob,data_path,value,group_name,frame=None,index = -1) :
    #print("frame_func",frame)
    if frame == None :
        frame = bpy.context.scene.frame_current

    interpolation = bpy.context.user_preferences.edit.keyframe_new_interpolation_type
    color_mode = 'AUTO_RGB'
    fc = get_fcurve(ob,data_path,index = index)

    group = get_group(ob,group_name)
    if fc.group != group :
        fc.group = group
    fc.color_mode = color_mode

    point = fc.keyframe_points.insert(frame,value)
    point.interpolation = interpolation
