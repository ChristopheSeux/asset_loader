from .functions import *
from .widgets import *
from .properties import AssetLoaderSettings
from bpy.props import *
from time import time,sleep
from .func_load import load_pose_3d,load_group,load_anim_3d
from .func_store import store_anim_3d, store_pose_3d
from glob import glob

class AssetLoaderWindow(bpy.types.Operator):
    """Open the asset loader"""
    bl_idname = "view3d.asset_loader"
    bl_label = "Asset Loader"

    inside = False
    mouse = (0,0)
    mouse_on_press = (0,0)
    active_btn = None
    TopBarLayout = None
    active_widget = None
    active_value = None
    new_value = 0
    event = None
    press_time =0

    blocking = False

    widgets = []

    store_view_prefs = {
        "show_object_info" : {"modal":False},
        "show_mini_axis"  : {"modal":False}
    }

    _timer = None


    def store_asset(self) :
        scene = bpy.context.scene
        ob = bpy.context.object

        asset_type = self.bl_props.asset_type
        extra_info = {}

        if asset_type == 'pose_3d' :
            bones = []
            print("Store Pose 3d")
            if self.bl_props.asset_selected_only :
                bones = bpy.context.selected_pose_bones

            options = {'ONLY_KEYED'}
            if self.bl_props.asset_mirror : options.add('MIRROR')
            asset = store_pose_3d(bones = bones,options = options)
            asset_dir = join(get_poselib_dir(),self.bl_props.folders)

            extra_info = {"dupli_group" : ob.proxy_group.dupli_group.name}

        elif asset_type == 'anim_3d' :
            #def
            print("Store Anim3d")
            select = self.bl_props.asset_selected_only
            start = scene.frame_start
            end = scene.frame_end
            if scene.use_preview_range :
                start = scene.frame_preview_start
                end = scene.frame_preview_end

            ##########store_acting(start,end,only_selected,bezier,mirror) :
            asset = store_anim_3d(start,end,select,True, self.bl_props.asset_mirror)
            asset_dir = join(get_poselib_dir(),self.bl_props.folders)

            extra_info = {"dupli_group" : ob.proxy_group.dupli_group.name}

        ##############STORING
        asset_name = self.bl_props.asset_name
        asset_txt_path = join(asset_dir,asset_name+'.txt')
        json_path = join(asset_dir,asset_name+'.json')
        image_path = join(asset_dir, asset_name+'.png')

        write_json(asset_txt_path, asset)

        pose_info={
            "type" : asset_type,
            "asset" : asset_name,
            "image" : "./%s.png"%(asset_name),
            "tags" : "",
            "path": "./%s.txt"%(asset_name),
            "description" : "",
            "bg_color" : (0.4, 0.4, 0.4, 0.85)}

        pose_info = {**pose_info,**extra_info}

        write_json(json_path, pose_info)

        # Save image
        #images = [i for i in self.SnapshotLayout.widgets if isinstance(i,Image)]
        print('Save Snapshot')
        for im in self.SnapshotLayout.widgets :
            im.save_image(image_path)




        #print('###')
        #print(images[0])
        #im = Image.fromarray(A)
        #im.save("your_file.jpeg")
        self.set_ui()

    def add_asset(self) :
        asset_types = [('pose_3d','SPACE2'),('anim_3d','ACTION'),('group','GROUP'),('material','MATERIAL')]
        AssetLoaderSettings.asset_type= EnumProperty(items = [(a,a.title(),"") for a,i in asset_types])
        AssetLoaderSettings.asset_name= StringProperty()
        AssetLoaderSettings.asset_selected_only = BoolProperty(default = True)
        AssetLoaderSettings.asset_mirror = BoolProperty(default = False)

        btn_snapshot = PushButton(self.make_snapshot,args = {},icon = bl_icon_path('RENDER_REGION'),text='')
        asset_types_items = [EnumItem(self.bl_props,'asset_type',a,text='',icon=bl_icon_path(i)) for a,i in asset_types]
        asset_name = String(self.bl_props,'asset_name',icon = bl_icon_path('SORTALPHA'))
        asset_selected_only = Boolean(self.bl_props,'asset_selected_only',text = 'Select Only')
        asset_mirror = Boolean(self.bl_props,'asset_mirror',text = 'Mirror')

        asset_name.minimum_width = 225
        btn_store_asset = PushButton(self.store_asset,text= '',icon = bl_icon_path('FILE_TICK'))

        add_asset_settings = [  Spacer(20), *asset_types_items,
                                Spacer(10), btn_snapshot, asset_name,
                                btn_store_asset, Spacer(10),
                                asset_selected_only,asset_mirror ]

        self.operator_list = list(bpy.context.window_manager.operators)
        self.show_settings = True
        self.settings_layout.setWidgets(add_asset_settings)


    def make_snapshot(self) :
        print('Make Snapshot')
        self.blocking = True
        snapshot = Snapshot(self)

    def set_modal(self):
        print('Cancel Modal')
        for prop,value in self.store_view_prefs.items() :
            self.store_view_prefs[prop]["store"] = getattr(self.bl_prefs.view, prop)
            setattr(self.bl_prefs.view,prop,value["modal"])

        return {'RUNNING_MODAL'}

    def cancel_modal(self) :
        print('Cancel Modal')
        for prop,value in self.store_view_prefs.items() :
            setattr(self.bl_prefs.view,prop,value["store"])

        self.bl_prefs.view.show_object_info = True
        self.running = False

        return {'CANCELLED'}

    def open_lib_dir(self):
        import subprocess, os

        ob = bpy.context.object

        path = get_lib_dir()
        if ob and ob.mode == 'POSE':  path = get_poselib_dir()

        try:
            os.startfile(path)
        except:
            subprocess.Popen(['xdg-open', path])
            pass

    def open_asset(self):
        import subprocess
        blender = bpy.app.binary_path

        selected_items = self.AssetList.selected_items

        if not selected_items :
            print("No asset selected")
            return

        item = selected_items[0]
        paths = glob(item.info["path"])

        if not paths :
            print("%s not exist"%item.info["path"])
            return

        path = paths[-1]

        subprocess.Popen([blender, path])


    def store_poselib(self):
        ob = bpy.context.object
        scene = bpy.context.scene
        space_data = bpy.context.space_data

        animation_data = ob.animation_data
        cam = bpy.context.scene.camera

        if not animation_data :
            print("The active object as no animation_data")
            return

        action = animation_data.action

        if not action :
            print("No active action")
            return

        library = ob.data.library
        if not library :
            print('The Active rig is not a reference')
            return

        if not cam :
            print("No active camera")
            return

        pose_name = action.name.replace('/','_')

        rig_dir = dirname(library_abspath(library.filepath))

        poseLib_path = join(rig_dir, 'poseLib',action.name)

        if not exists(poseLib_path) :
            os.mkdir(poseLib_path)

        #set display
        scene.render.resolution_x = 140
        scene.render.resolution_y = 140
        scene.render.antialiasing_samples = '16'
        scene.render.use_full_sample = True
        scene.render.resolution_percentage =100

        space_data.region_3d.view_perspective = 'CAMERA'
        space_data.show_only_render = True
        space_data.viewport_shade = 'MATERIAL'
        space_data.fx_settings.use_ssao = True

        bpy.context.scene.render.alpha_mode = 'TRANSPARENT'


        for m in action.pose_markers :
            f = m.frame
            scene.frame_set(f)

            pose_name = m.name.replace('/','_')

            #pose = store_acting(f,f,only_selected = False,bezier=False,mirror = False)
            pose = store_pose_3d()

            pose_txt_path = join(poseLib_path,pose_name+'.txt')
            json_path = join(poseLib_path,pose_name+'.json')
            image_path = join(poseLib_path, pose_name+'.png')

            scene.render.filepath = image_path

            #with open(pose_txt_path, "w") as f:
            #    f.write(str(pose))
            write_json(pose_txt_path, pose)

            pose_info={
                "type" : "pose_3d",
                "asset" : pose_name,
                "image" : "./%s.png"%(pose_name),
                "tags" : "",
                "path": "./%s.txt"%(pose_name),
                "description" : "",
                "bg_color" : (0.4, 0.4, 0.4, 0.85),
                "dupli_group" : ob.proxy_group.dupli_group.name
                        }

            #print(pose_info)

            write_json(json_path, pose_info)

            bpy.ops.render.opengl(write_still = True)

        # Restore view settings

        space_data.show_only_render = False
        space_data.viewport_shade = 'SOLID'


    def store_groups(self, blend_files,replace_json=False,replace_image=False):
        import subprocess

        blender_path = bpy.app.binary_path
        template = join(dirname(__file__),"preview_generator.blend")

        batch = join(batch_folder,"store_groups.py")

        store = env_vars["STORE"]
        lib = get_lib_dir()

        files = []
        for i,file in enumerate(blend_files) :
            files.append(file)
            if i%3 == 0 :
                args = {"files_info": files,"store":store,"lib_path":lib,"replace_json":replace_json,"replace_image":replace_image}
                subprocess.call([blender_path,template,'-p','4096','0','0','0','--python',batch,'--',str(args)])
                files = []


    def get_lib_items(self) :
        for dir in self.folders:
            dir_item = EnumItem(self.bl_props,"folders",dir.name,text = dir.name.title())
            dir_item.dir_name = dir.name
            self.folder_items.append(dir_item)

            for f in sorted([f for f in scantree(dir.path) if f.name.endswith('json')],key = lambda x :x.name) :
                json_name = f.name
                json_path = f.path

                chdir(dirname(json_path))
                #print(item_info)
                item_info = read_json(json_path)

                if not item_info :
                    print("the json %s cannot be read"%item_info)
                    continue

                if '{STORE}' in item_info["path"] :
                    item_info['path'] = item_info["path"].format(STORE = self.store)

                item_info["path"] = abspath(item_info["path"])


                image_path = abspath(item_info["image"])

                item = Image(image_path,size = (140,140))
                item.setText(splitext(json_name)[0])
                item.info = item_info
                item.dir = dir.name
                item.connect = (self.load_asset ,{"link" : True})

                self.asset_items.append(item)

    def get_pose_items(self) :
        ob = bpy.context.object

        if not ob.proxy_group :
            print('Active Rig is not a reference')
            return

        dupli_group = ob.proxy_group.dupli_group.name

        for dir in self.folders :
            dir_label = dir.name
            if dir.name.startswith(dupli_group+'_') :
                dir_label = dir.name.replace(dupli_group+'_',"")

            dir_item = EnumItem(self.bl_props,"folders",dir.name,text = dir_label.title())
            dir_item.dir_name = dir.name
            self.folder_items.append(dir_item)

            for f in sorted([f for f in scantree(dir.path) if f.name.endswith('json')],key = lambda x :x.name) :
                json_name = f.name
                json_path = f.path

                chdir(dirname(json_path))
                #print(item_info)
                item_info = read_json(json_path)

                if not item_info : continue


                if '{STORE}' in item_info["path"] :
                    item_info['path'] = item_info["path"].format(STORE = self.store)

                item_info["path"] = abspath(item_info["path"])


                image_path = abspath(item_info["image"])

                if not exists(image_path) : image_path = icon_path("empty_image")

                item = Image(image_path,size = (140,140))
                item.setText(splitext(json_name)[0])
                item.info = item_info
                item.dir = dir.name
                item.connect = self.load_asset

                self.asset_items.append(item)

                print(image_path)


    def draw_callback(self, context):
        if context.area != self.area :  return

        Widget.region_size = (context.area.regions[4].width,context.area.regions[4].height)
        print('1')

        print('###')
        print(self.asset_items)
        for asset_item in self.asset_items :
            print(self.bl_props.search,asset_item.text,asset_item.dir,self.bl_props.folders)
            if self.bl_props.search in asset_item.text and asset_item.dir == self.bl_props.folders:
                asset_item.hide = False
                #asset_item.select = False
            else :
                asset_item.hide  = True
                asset_item.select = False

        print('2')
        if not self.show_settings :
        #    #print("show settings",self.show_settings)
            self.settings_layout.setWidgets([])

        print('3')
        #print(self.Heself.HeaderLayout.widgetsaderLayout.widgets)
        self.WindowLayout.draw()

        for w in self.widgets :
            w.draw()

        print('4')
        #print(self.SnapshotLayout.widgets)

        #self.TopBarLayout.draw()

        center_x = bpy.context.area.regions[4].width /2
        center_y = bpy.context.area.regions[4].height /2

        #x1 = center_x -self.texture["dimensions"][0]/2
        #y1 = center_y -self.texture["dimensions"][1]/2
        #x2 = x1 + self.texture["dimensions"][0]
        #y2 = y1 + self.texture["dimensions"][1]

        #self.img.draw()
        #glEnable(GL_BLEND)
        #glEnable(GL_TEXTURE_2D)

    def get_mouse_event(self,event):
        self.mouse = (event.mouse_region_x,event.mouse_region_y)
        self.mouse_window = (event.mouse_x,event.mouse_y)
        #self.active_widgets = Widget.active_widgets
        Widget.mouse = self.mouse
        Widget.mouse_window = self.mouse_window
        Widget.event_type = event.type
        Widget.event_value = event.value
        Widget.modifiers = {'shift' : event.shift,'ctrl':event.ctrl,'alt' : event.alt}

        mouse_buttons = ('left_mouse','right_mouse','middle_mouse')

        for mb in mouse_buttons :
            m = getattr(Widget, mb)
            MB = mb.upper().replace('_','')

            m.press = False
            m.release = False
            m.double_click = False

            m.press_count = 0

            if m.tmp_down != m.down :
                m.tmp_down = m.down

                if m.down :
                    m.press = True
                else :
                    m.release = True

            if event.type == MB and event.value == 'PRESS' :
                m.release = False
                m.down = True
                m.press = True

                if (time() - m.press_time)< 0.2 :
                    m.double_click = True
                    delay = time() - m.press_time
                    sleep(delay)

                m.press_time = time()

                #m.wait_click = True


            elif event.type == MB and event.value == 'RELEASE' :
                m.down = False
                m.press = False
                m.release = True
                #m.double_click = False





        Widget.ascii = event.ascii

    def refresh_pose_3d(self) :
        #print("frame",self.apply_pose_frame)
        load_pose_3d(self.pose_3d,selected_only=False,insert_keyframe = True)


        bpy.context.scene.update()

        load_pose_3d(
            self.pose_3d_info,
            blend = self.bl_props.pose_blend,
            left =self.bl_props.pose_left,
            right = self.bl_props.pose_right,
            selected_only = self.bl_props.pose_selected_only,
            frame = self.apply_pose_frame)




    def load_asset(self,link=True) :
        from glob import glob
        scene = bpy.context.scene
        ob = bpy.context.object

        #print([i.info for i in self.AssetList.selected_items])

        print('INFO',[i.info for i in self.AssetList.selected_items])
        print("")

        for i in self.AssetList.selected_items :
            info = i.info.copy()

            paths = sorted(glob(info["path"]))

            if not paths :
                print('asset %s not found' % i.info["path"])
                continue

            info['path'] = paths[-1]

            if not exists(info['path']) :
                print('The path %s not exist'%info['path'])
                continue


            if info["type"] == 'groups' :
                group,empty,proxy = load_group(info["path"],info["asset"],link= link)

                if not proxy :
                    empty.location = scene.cursor_location

            elif info["type"] == 'materials' :
                pass
            elif info["type"] == 'node_groups' :
                pass

            elif info["type"] == 'anim_3d' :
                if not ob :
                    print("No active Object")
                    return

                # Bl properties
                #AssetLoaderSettings.pose_blend = IntProperty(subtype= 'PERCENTAGE',min = 0, max = 100,default = 100)
                AssetLoaderSettings.pose_right = BoolProperty(default = True)
                AssetLoaderSettings.pose_left = BoolProperty(default = True)
                AssetLoaderSettings.pose_selected_only = BoolProperty(default = True)
                AssetLoaderSettings.pose_mirror = BoolProperty(default = True)

                self.anim_3d_info = read_json(info['path'])

                load_anim_3d(self.anim_3d_info,
                    blend = 1,
                    left= self.bl_props.pose_left,
                    right= self.bl_props.pose_right,
                    selected_only= self.bl_props.pose_selected_only,
                    mirror = self.bl_props.pose_mirror,
                    frame = scene.frame_current)

                ### Apply pose btn
                self.bool_right = Boolean(self.bl_props,"pose_right",text = 'R')
                self.bool_left = Boolean(self.bl_props,"pose_left",text = 'L')
                self.bool_selected_only = Boolean(self.bl_props,"pose_selected_only",text = 'Select Only')
                self.bool_mirror = Boolean(self.bl_props,"pose_mirror",text = 'Mirror')

                pose_settings = [Spacer(10),self.bool_right,self.bool_left,Spacer(5),self.bool_selected_only,self.bool_mirror]

                self.operator_list = list(bpy.context.window_manager.operators)
                self.settings_layout.setWidgets(pose_settings)


            elif info["type"] == 'pose_3d' :
                if not ob :
                    print("No active Object")
                    return


                # Bl properties
                AssetLoaderSettings.pose_blend = IntProperty(subtype= 'PERCENTAGE',min = 0, max = 100,default = 100)
                AssetLoaderSettings.pose_right = BoolProperty(default = True)
                AssetLoaderSettings.pose_left = BoolProperty(default = True)
                AssetLoaderSettings.pose_selected_only = BoolProperty(default = True)


                self.pose_3d_info = read_json(info['path'])

                #print('path',info['path'])

                bones =  [ob.pose.bones.get(b) for b in self.pose_3d_info.keys()]
                bones = [b for b in bones if b is not None]


                self.pose_3d = store_pose_3d(options = {"ALL"}, bones = bones)

                #print('pose_3d',self.pose_3d.keys())

                self.apply_pose_frame = scene.frame_current

                #print("frame",self.apply_pose_frame)
                #self.bl_props.pose_blend = 100
                self.bl_props.pose_blend = 100

                #print('info',self.pose_3d_info.keys())

                load_pose_3d(
                    self.pose_3d_info,
                    blend = 100,
                    left =self.bl_props.pose_left,
                    right = self.bl_props.pose_right,
                    selected_only = self.bl_props.pose_selected_only)


                #self.refresh_pose_3d()



                ### Apply pose btn
                self.sld_blend = Slider(self.bl_props,"pose_blend",text = 'Blend')
                self.sld_blend.minimum_width = 120
                self.sld_blend.connect = self.refresh_pose_3d

                self.bool_right = Boolean(self.bl_props,"pose_right",text = 'R')
                self.bool_right.connect = self.refresh_pose_3d

                self.bool_left = Boolean(self.bl_props,"pose_left",text = 'L')
                self.bool_left.connect = self.refresh_pose_3d

                self.bool_selected_only = Boolean(self.bl_props,"pose_selected_only",text = 'Select Only')
                self.bool_selected_only.connect = self.refresh_pose_3d

                pose_settings = [Spacer(10),self.sld_blend,Spacer(5),self.bool_right,self.bool_left,Spacer(5),self.bool_selected_only]

                self.operator_list = list(bpy.context.window_manager.operators)
                self.settings_layout.setWidgets(pose_settings)

        self.show_settings = True

        bpy.ops.ed.undo_push()
        #self.AssetList
        #load_args = {}
        #path,asset_type,asset,link=True) :

    def set_ui(self) :
        scene = bpy.context.scene
        ob = bpy.context.object

        width = bpy.context.area.regions[4].width
        height = bpy.context.area.regions[4].height

        #center_x = int(width /2)
        #center_y = int(height)-20

        mouse = self.mouse

        pos = (30,height-30)

        self.WindowLayout = Layout(direction = 'VERTICAL',expand = True)


        btn_append = PushButton(self.load_asset,args = {"link" : False},icon = bl_icon_path('APPEND_BLEND'),text='Append')
        btn_open = PushButton(self.open_asset,icon = bl_icon_path('FILE_FOLDER'),text='Open')

        spacer = Spacer()
        spacer5 = Spacer(5)
        spacer4 = Spacer(4)

        self.folder_items = []
        self.asset_items = []

        ##self.snapshot = bpy.data.images.get('snapshot')
        #if not self.snapshot :
        #    self.snapshot = bpy.data.images.new('snapshot',128,128)

        #self.snapshot.scale(128, 128)

        if ob and ob.mode == 'POSE' :
            poseLib_path = get_poselib_dir()

            group_name = ob.proxy_group.dupli_group.name

            import re
            if re.search('_[0-9]{3}',group_name) :
                group_name = group_name[:-4]

            self.folders = [f for f in scandir(poseLib_path) if f.is_dir() and f.name.rsplit('_',1)[0]==group_name]
            AssetLoaderSettings.folders =  EnumProperty(items = [(f.name,f.name,"") for f in self.folders])

            print([f.name for f in self.folders])
            self.get_pose_items()


            btn_link = PushButton(self.load_asset,icon = bl_icon_path('ACTION'),text='Apply Pose')

            context_btn = [btn_link]


        else :
            self.folders = [f for f in scandir(self.lib_dir) if f.is_dir()]
            AssetLoaderSettings.folders =  EnumProperty(items = [(f.name,f.name,"") for f in self.folders])
            self.get_lib_items()

            btn_link = PushButton(self.load_asset, args = {"link" : True},icon = bl_icon_path('LINK_BLEND'),text='Link')
            btn_make_proxy = Boolean(self.bl_props, "make_proxy",text="Make Proxy",icon = bl_icon_path('ARMATURE_DATA'))
            context_btn = [btn_link,btn_append,btn_open]

        self.AssetList = WidgetList(items = self.asset_items)
        self.AssetList.context_menu = Menu(items = context_btn)


        #print(btn_link.is_over)



        #self.AssetBar.setLayout(self.AssetBarLayout)

        #self.AssetLayout.addWidget(self.AssetList )
        #self.AssetLayout.addWidget(self.spacer0 )




        self.SnapshotFrame = Frame(self)
        self.SnapshotFrame.minimum_height = 124
        self.SnapshotFrame.color = (0.5,0.5,0.5,0) # TRANSPARENT
        self.SnapshotFrame.border_color = (0.5,0.5,0.5,0) # TRANSPARENT
        self.SnapshotLayout = Layout(margin = (10,0),spacing = 0)
        self.SnapshotFrame.setLayout(self.SnapshotLayout)

        self.CatFrame = Frame(self)
        self.CatFrame.color = (0.5,0.5,0.5,0) # TRANSPARENT
        self.CatFrame.border_color = (0.5,0.5,0.5,0) # TRANSPARENT
        self.CatFrame.minimum_height = 23
        self.CatLayout = Layout(margin = (2,0),spacing = 1)
        self.CatFrame.setLayout(self.CatLayout)

        self.HeaderFrame = Frame(self)
        self.HeaderFrame.color = (0.5,0.5,0.5,0.75)
        self.HeaderFrame.minimum_height = 33
        self.HeaderLayout = Layout(margin = (10,5),spacing = 0)
        self.HeaderFrame.setLayout(self.HeaderLayout)


        self.WindowLayout.addWidget(spacer)
        self.WindowLayout.addWidget(self.SnapshotFrame)
        self.WindowLayout.addWidget(self.CatFrame)
        self.WindowLayout.addWidget(self.HeaderFrame)
        self.WindowLayout.addWidget(self.AssetList)

        #slider = Slider(bpy.context.scene.render,"resolution_percentage")
        #slider2 = Slider(bpy.context.scene.render.image_settings,"compression")


        #op = PushButton("gpencil.draw",icon = icon_path('ICON_gpencil_draw'),text='')
        self.refresh = PushButton(self.set_ui,icon = bl_icon_path('FILE_REFRESH'),text='')

        #self.add = PushButton(self.add_assets,args = {},icon = bl_icon_path('ZOOMIN'),text='')
        #self.add_poselib = PushButton(self.store_poselib,args = {},icon = bl_icon_path('ZOOMIN'),text='')

        self.btn_add_asset = PushButton(self.add_asset,icon = bl_icon_path('ZOOMIN'),text='')


        self.open_lib = PushButton(self.open_lib_dir,args = {},icon = bl_icon_path('FILESEL'),text='')
        #props = Boolean(scene.render,"use_border",text = 'Props')
        #perso = Boolean(scene.render,"use_border",text = 'Persos')
        #decors = Boolean(scene.render,"use_border",text = 'Decors')


        self.search = String(self.bl_props,"search",icon = bl_icon_path('VIEWZOOM'))
        self.search.connect = self.AssetList.reset_scroll
        self.search.minimum_width = 225
        self.HeaderLayout.addWidget(self.refresh)
        self.HeaderLayout.addWidget(self.search)

        self.HeaderLayout.addWidget(self.open_lib)
        #self.HeaderLayout.addWidget(spacer5)

        self.HeaderLayout.addWidget(self.btn_add_asset)

        ## For apply pose
        #self.HeaderLayout.addWidget(spacer)
        self.settings_layout = Layout()

        #self.settings_layout.addWidget(self.sld_blend)
        self.HeaderLayout.addWidget(self.settings_layout)
        self.HeaderLayout.addWidget(spacer)
        #self.HeaderLayout.addWidget(spacer)

        self.CatLayout.addWidget(spacer)
        for folder_item in self.folder_items :
            folder_item.connect= self.AssetList.reset_scroll
            self.CatLayout.addWidget(folder_item)
            #self.CatLayout.addWidget(spacer4)



    def modal(self, context, event):
        #Widget.event = event
        #self.event = event
        self.get_mouse_event(event)

        if context.area != self.area : return {'PASS_THROUGH'}

        context.area.tag_redraw()

        #print(event.value)

        if self.operator_list != list(bpy.context.window_manager.operators) :
            self.show_settings = False

        Widget.region_size = (context.area.regions[4].width,context.area.regions[4].height)

        #print(self.AssetList.widtg,self.AssetList.pos_x)
        #print(self.region.width,self.region.height)
        #print(context.active_operator)

        #print(self.HeaderLayout.is_over,self.AssetList.is_over,Widget.active_widgets)
        #print(self.HeaderLayout.is_over,self.AssetList.is_over,Widget.active_widgets)
        #print(self.mouse[1])
        if self.blocking or self.CatLayout.is_over or self.HeaderLayout.is_over or self.AssetList.is_over or Widget.active_widgets :
            return {'RUNNING_MODAL'}

        if event.type in {'ESC'} :
            bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
            self.cancel_modal()
            return {'CANCELLED'}


        #if self.HeaderFrame.is_over:
        #    return {'RUNNING_MODAL'}

        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        wm = context.window_manager

        self.bl_prefs = context.user_preferences
        self.addon_prefs = self.bl_prefs.addons[__package__].preferences
        self.bl_props = getattr(context.scene,__package__)

        #print("package",__package__)
        #print("lib_dir",__package__)
        self.running = True

        self.area = context.area


        self.region = context.area.regions[4]
        self.width = self.region.width
        self.height = self.region.height

        Widget.area = context.area

        self.mouse = (event.mouse_region_x,event.mouse_region_y)


        self.event_type = event.type
        self.event_value = event.value

        self.show_settings = False
        self.operator_list = list(bpy.context.window_manager.operators)
        self.apply_pose_frame =None

        #self._timer = wm.event_timer_add(time_step=0.01, window=context.window)

        self.store = get_store()

        if not self.store :
            print("No store in the prefs")
            return self.cancel_modal()

        self.lib_dir = get_lib_dir()

        if not self.lib_dir :
            return self.cancel_modal()


        self.folders = []





        #self.bl_props['asset_folders'] = [d for d in listdir(self.lib_dir) if isdir(join(self.lib_dir,d))]


        self.set_ui()

        if context.area.type == 'VIEW_3D':

            self.set_modal()
            # the arguments we pass the the callback
            args = ( context,)
            # Add the region OpenGL drawing callback
            # draw in view space with 'POST_VIEW' and 'PRE_VIEW'
            self._handle = bpy.types.SpaceView3D.draw_handler_add(self.draw_callback, args, 'WINDOW', 'POST_PIXEL')

            wm.modal_handler_add(self)
            return {'RUNNING_MODAL'}
        else:
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return self.cancel_modal()
