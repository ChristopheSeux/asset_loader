from .Property import Property
from .Widget import Widget
from .PushButton import PushButton
from .Picture import Picture
from ..functions import *

class String(Property):
    def __init__(self,data,property,text=None,icon= None):
        self.state = 'DEFAULT'

        self.data= data
        self.property = property

        #icon = self.icon

        self.timer = False
        self.time = time.time()

        #self.modifiers = {}

        self.text_index = 0
        self.icon_offset = 18

        self.selected_chars = [0,0]


        self.properties = self.data.bl_rna.properties[self.property]


        self.value = getattr(data,property)
        self.previous_value = self.value

        if text is None:
            self.setText(self.property.replace('_',' ').title())
        else :
            self.setText(text)

        if icon :
            self.setIcon(icon)

        self.btn_clear_text = PushButton(self.clear_text, {},icon = bl_icon_path('PANEL_CLOSE'))
        self.btn_clear_text.run_on_press = True
        #self.close_icon = Picture(bl_icon_path("PANEL_CLOSE"),size =(20,20))


    @property
    def text_pos(self) :
        pos = self.bound_box[3] + self.margin
        if self.icon :
            pos = pos+Vector((self.icon_offset,0))

        return pos


    def setMinimumSize(self,size = None) :
        if size is None :
            if self.icon and not self.text :
                self.minimum_width =  22
                return
            dim = 2*self.margin[0]
            text_dim = self.getTextSize(self.text)
            dim+= text_dim[0]

            if self.icon :
                dim += 10

            self.minimum_width =  dim
        else :
            self.minimum_width =  size

    def clear_text(self) :
        setattr(self.data,self.property,"")
        self.value = ""
        pass

    def textEdit(self) :
        if self.is_pressed :
            if self.event_value =='PRESS' :
                if time.time() - self.time >= 0.028 :
                    self.time = time.time()
                else :
                    return

                if self.ascii : # key is a number
                    self.value = self.value[0:self.text_index]+ self.ascii+ self.value[self.text_index:]
                    setattr(self.data,self.property,self.value)
                    self.text_index +=1

                if self.event_type == 'LEFT_ARROW' and self.text_index !=0 :
                    self.text_index -= 1

                elif self.event_type == 'RIGHT_ARROW' and self.text_index != len(self.value) :
                    self.text_index +=1

                elif self.event_type == 'BACK_SPACE' and self.text_index != 0 :
                    self.value = self.value[0:self.text_index-1]+self.value[self.text_index:]
                    setattr(self.data,self.property,self.value)
                    self.text_index -=1

                elif self.event_type == 'DEL' and self.text_index != len(self.value) :
                    self.value = self.value[0:self.text_index]+self.value[self.text_index+1:]
                    setattr(self.data,self.property,self.value)


            elif self.event_value =='RELEASE' :
                self.timer = True


    def processEvent(self) :
        self.isMouseOver()
        self.boundBox()

        # Click on the widget
        if self.isKeyPress('LEFTMOUSE') :
            if self.is_over and not self.is_pressed :
                self.previous_value = self.value
                self.text_index = len(self.value)
                self.is_pressed = True

                self.selected_chars = [0,len(self.value)]
                Widget.active_widgets = [self]

                bpy.context.window.cursor_set("TEXT")

                if self.connect :
                    self.connect()


            if self.is_pressed and not self.is_over :
                #print('Exit')
                self.is_pressed = False
                Widget.active_widgets = []
                bpy.context.window.cursor_set("DEFAULT")

        # Move text Cursor
        if self.is_pressed and self.left_mouse.down :

            char_pos = [self.getTextSize(self.value[:i])[0]+self.text_pos[0] for i in range(len(self.value)+1)]
            char_dist = min(char_pos, key = lambda x : abs(x-self.mouse[0]))
            self.text_index = char_pos.index(char_dist)

            self.selected_chars = [0,len(self.value)]

        # Valid text
        if self.event_type in ('RET','NUMPAD_ENTER') and self.event_value =='PRESS' :
            self.is_pressed = False
            Widget.active_widgets = []
            bpy.context.window.cursor_set("DEFAULT")

        # Cancel text
        elif self.is_pressed and self.event_type in ('ESC','RIGHTMOUSE') and self.event_value =='PRESS' :
            self.is_pressed = False
            Widget.active_widgets = []
            setattr(self.data,self.property,self.previous_value)
            bpy.context.window.cursor_set("DEFAULT")

        self.textEdit()

    def draw(self) :
        self.value = getattr(self.data,self.property)
        self.processEvent()


        glEnable(GL_BLEND)


        box = self.bound_box

        #rim
        glColor4f(1, 1, 1, 0.15)

        #print(upper_left,self.rim_offset)
        offset_rim_vector = Vector((self.rim_offset,-self.rim_offset))
        draw_square(*(box+offset_rim_vector))
        #draw_square(upper_left,upper_right,lower_right,lower_left)


        #background default color
        bg_color_top = (0.58, 0.58, 0.58, 1)
        bg_color_bottom = (0.69, 0.69, 0.69, 1)

        #Background mouse hover
        #if self.value :
        #bg_color_bottom = self.changeColor(bg_color_bottom,self.dark_offset*4)
        #bg_color_top= self.changeColor(bg_color_top,self.dark_offset*4)


        if self.is_over and not self.is_pressed :
            bg_color_top = self.changeColor(bg_color_top,self.bright_offset)
            bg_color_bottom = self.changeColor(bg_color_bottom,self.bright_offset)


        draw_gradient(*box,bg_color_top,bg_color_bottom)


        bg_color_top = (0.5, 0.5, 0.5, 1)
        bg_color_bottom = (0.42, 0.42, 0.42, 1)

        #outline mouse hover
        if self.is_over and not self.is_pressed :
            bg_color_top = self.changeColor(bg_color_top,self.bright_offset)
            bg_color_bottom = self.changeColor(bg_color_bottom,self.bright_offset)


        #contour
        glLineWidth(1)
        draw_square(*box,color=(0.2, 0.2, 0.2, 1), primitive = GL_LINE_STRIP)


        if self.is_pressed :
            glColor3f(1,1,1)
        else :
            glColor3f(0.05,0.05,0.05)

        text_pos = self.text_pos

        """
         # Draw highlight text background
        if self.is_pressed :

            highlight_text_color = (0.5,0.5,0.5,1)
            #print(self.selected_chars[0])

            #highlight_text_color = (1,0,0,1)


            text_start = self.getTextSize(self.value[:self.selected_chars[0]])[0]
            text_width = self.getTextSize(self.value[self.selected_chars[0]:self.selected_chars[1]])[0]


            p1 = box[0] + Vector((text_start+self.margin[0]+self.icon_offset,-2))
            p2 = box[3] + Vector((text_start+self.margin[0]+self.icon_offset,2))
            p3 = p2 + Vector((text_width,0))
            p4 = p1 + Vector((text_width,0))

            glBegin(GL_POLYGON)
            draw_square(p1,p2,p3,p4,highlight_text_color)
            glEnd()
        """

        # Draw Text
        if self.is_pressed :
            glColor4f(1, 1, 1, 1)
        else :
            glColor4f(0.05, 0.05, 0.05, 1)

        #blf.enable(self.font_id, blf.WORD_WRAP)
        #blf.word_wrap(self.font_id, 20)
        blf.position(self.font_id, *text_pos, 0)
        blf.size(self.font_id, 12, 72)
        blf.draw(self.font_id, self.value)

        blf.disable(self.font_id, blf.WORD_WRAP)

        #print(self.value[:self.text_index])
        dim = self.getTextSize(self.value[:self.text_index])

        p1 = box[0] + Vector((dim[0]+self.margin[0]+self.icon_offset,-2))
        p2 = box[3] + Vector((dim[0]+self.margin[0]+self.icon_offset,2))

        glColor4f(0.15,0.45,1.0,0.5)
        glLineWidth(2)
        # Draw Cursor :
        if self.is_pressed :
            glBegin(GL_LINE_STRIP)
            glVertex2f(*p1)
            glVertex2f(*p2)

            glEnd()

        if self.icon :
            icon_center = (self.height -self.icon.size[1])/2
            self.icon.pos = box[3] + Vector((icon_center+1,icon_center))
            self.icon.draw()


        if self.value :
            #print("draw btn")
            self.btn_clear_text.width = 23
            #self.btn_clear_text.height = 30
            self.btn_clear_text.pos_x = box[2][0] - 23
            self.btn_clear_text.pos_y = box[1][1]
            self.btn_clear_text.draw()



        # restore opengl defaults
        glLineWidth(1)
        glDisable(GL_BLEND)
        glColor4f(0.0, 0.0, 0.0, 1.0)
