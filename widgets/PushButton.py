from .Widget import Widget
from .Picture import Picture
from ..functions import *

class PushButton(Widget) :
    margin= (10,6)
    minimum_height = 24

    style = {
        "rim_color" : (1, 1, 1, 0.15),
        "rim_offset" : 1,
        "bg_color" : (0.65, 0.65, 0.65, 1),
        "text_color" : (0.95,0.95,0.95,1),
        "text_pressed_color" : (0.05,0.05,0.05,1),
        "border_width" : 1,
        "border_color" : (0.15,0.15,0.15,1),
        }

    run_on_press = False


    def __init__(self,action,args = {},text=None,icon=None):
        self.action = action
        self.args = args

        if text is None :
            #self.setText(self.id_name.split('.')[-1].replace('_',' ').title())
            self.setText("")
        else :
            self.setText(text)


        if icon :
            self.setIcon(icon)

        self.style = self.style.copy()


    def run_action(self) :
        if isinstance(self.action,str) :
            op_cat,op_name = self.action.split('.')
            action = getattr(getattr(bpy.ops,op_cat),op_name)

            action('INVOKE_DEFAULT')

        else :
            self.action(**self.args)



    def processEvent(self) :
        self.isMouseOver()
        self.boundBox()

        #print(self.event_value)
        #print("click",self.left_mouse.click)
        #print("press",self.left_mouse.press)
        #print("double_click",self.left_mouse.double_click)


        if self.is_over and not self.is_pressed:
            if self.left_mouse.press :
                self.is_pressed = True

                if self.run_on_press :
                    self.run_action()
                    self.is_pressed = False


        if self.left_mouse.release:
            if self.is_pressed and self.is_over and not self.run_on_press:
                self.run_action()
            self.is_pressed = False




    def draw(self) :
        glEnable(GL_BLEND)

        self.processEvent()

        box = self.bound_box

        if not self.style.get("bg_select_color") :
            self.style["bg_select_color"] = self.changeColor(self.style["bg_color"],self.bright_offset)

        #print(box)

        #rim
        glColor4f(*self.style["rim_color"])

        #print(upper_left,self.rim_offset)
        offset_rim_vector = Vector((self.style["rim_offset"],-self.style["rim_offset"]))

        draw_square(*(box+offset_rim_vector),color = self.style["rim_color"])
        #draw_square(upper_left,upper_right,lower_right,lower_left)


        #background default color
        bg_color_top = self.changeColor(self.style["bg_color"],self.bright_offset)
        bg_color_bottom = self.changeColor(self.style["bg_color"],self.dark_offset)


        #Background mouse hover
        if self.is_pressed :
            bg_color_bottom = self.changeColor(bg_color_top,self.dark_offset*3)
            bg_color_top=    self.changeColor(bg_color_bottom,self.dark_offset*3)

        elif self.is_over:
            bg_color_top = self.changeColor(self.style["bg_select_color"],self.bright_offset)
            bg_color_bottom = self.changeColor(self.style["bg_select_color"],self.dark_offset)


        draw_gradient(*box,bg_color_top,bg_color_bottom)

        if self.style['border_width'] >0 :
            #contour
            glColor4f(*self.style['border_color'])
            glLineWidth(self.style['border_width'])


            draw_square(*box,color =self.style['border_color'], primitive = GL_LINE_STRIP)



        if self.is_pressed :
            glColor4f(*self.style['text_pressed_color'])
        else :
            glColor4f(*self.style['text_color'])


        if not self.icon :
            tex_pos = (box[3]+box[2])/2
        else :
            tex_pos = (box[3]+Vector((10,0))+box[2])/2


        tex_pos += Vector((0,self.margin[1]))
        tex_pos -= Vector((self.getTextSize(self.text)[0]/2,0))
        #tex_pos = box[2]
        # draw op name
        blf.position(self.font_id, *tex_pos, 0)
        blf.size(self.font_id, 12, 72)
        blf.draw(self.font_id, self.text)

        #Draw Icon
        icon_center = (self.height -self.icon.size[1])/2
        self.icon.pos = box[3] + Vector((icon_center,icon_center))
        self.icon.draw()


        # restore opengl defaults
        glLineWidth(1)
        glDisable(GL_BLEND)
        glColor4f(0.0, 0.0, 0.0, 1.0)
        glDisable(GL_TEXTURE_2D)
