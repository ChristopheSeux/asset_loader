from .Widget import Widget
from .Picture import Picture
from ..functions import *

class Frame(Widget):

    layout = None
    color = (0.5,0.5,0.5,0.5)
    border_color = (0.15,0.15,0.15,1)
    border_width = 1

    def __init__(self,parent = None) :
        self.parent = parent

    def processEvent(self) :

        #self.pos_x  = 50
        #self.width = self.region_size[0]
        #self.height = 100
        #self.pos_y = self.region_size[1]

        self.isMouseOver()
        self.boundBox()

    def setLayout(self,layout) :
        self.layout = layout
        #layout.width = self.width
        #layout.width = self.width

    def draw(self) :

        self.processEvent()



        #print(self.modal.mouse)
        #self.isMouseOver()
        glEnable(GL_BLEND)

        #upper_left,upper_right,lower_right,lower_left = self.bound_box

        #print(self.color)
        #glColor4f(*self.color)

        #glBegin(GL_POLYGON)
        draw_square(*self.bound_box,color = self.color)
        #glEnd()

        #if self.style :
        #glColor4f(0.15,0.15,0.15,1)
        glLineWidth(self.border_width)
        draw_square(*self.bound_box,color = self.border_color,primitive = GL_LINE_LOOP)
        """
        glBegin(GL_LINE_LOOP)

        glVertex2f(*self.bound_box[0])
        glVertex2f(*self.bound_box[1])
        glVertex2f(*self.bound_box[2])
        glVertex2f(*self.bound_box[3])
        glEnd()
        """

        if self.layout :
            self.layout.width = self.width
            self.layout.height = self.height
            self.layout.pos_x = self.pos_x
            self.layout.pos_y = self.pos_y

            self.layout.draw()
