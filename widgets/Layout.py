from .Widget import Widget
from .Property import Property
from .Picture import Picture
from .Spacer import Spacer
from .Boolean import Boolean
from ..functions import *

class Layout(Widget):

    def __init__(self,margin = (0,0),spacing = 0,offset = (0,0),direction = 'HORIZONTAL',expand = False):
        self.widgets = []
        self.direction = direction

        #self.widget_size = 100
        #self.pos_x = pos[0]
        #self.pos_y = pos[1] + 5

        self.margin = margin
        self.spacing = spacing
        self.expand = expand

        self.offset = Vector(offset)
        #self.width = width
        #self.height = height

        #self.draw()

    def addWidget(self,widget):
        self.widgets.append(widget)
        if not self.expand :
            self.minimum_width += widget.minimum_width

    def setWidgets(self,widgets) :
        self.minimum_width =0
        self.widgets = []
        for w in widgets :
            self.addWidget(w)

    def draw(self) :
        #print(self.modal.mouse)
        self.isMouseOver()

        #if not self.widgets : return
        #self.pos_x = 0
        #self.pos_y = 0

        #print(self.region_size[0])

        #self.width = self.region_size[0]
        #self.height = self.region_size[1]
        #self.height = 80
        #print([w.minimum_width for w in self.widgets])
        #print(self.region_size[0])
        if self.widgets :
            spacing = (len(self.widgets)-2)*self.spacing

        if self.direction == 'HORIZONTAL' :
            if self.expand :
                #self.width = self.region_size[0]
                self.height = self.region_size[1]

            if not self.widgets :  return



            widgets_width = sum([w.minimum_width for w in self.widgets])

            #offset_x = (self.width-2*self.margin[0]) / self.widgets

            spacers = [w for w in self.widgets if isinstance(w,Spacer) and not w.minimum_width]

            #print(len(spacers))
            free_space = (self.width-2*self.margin[0]-spacing) - widgets_width

            if spacers :
                spacer_expand = free_space/len(spacers)
            #widget_len =

            #print(self.pos_x)

            pos_x = self.margin[0]
            for w in self.widgets :
                if isinstance(w,  (Widget,Property)) :
                    if w.hide :
                        continue
                    #print(w)

                    w.pos_x = self.pos_x + pos_x + self.offset[0]
                    w.pos_y = self.pos_y- self.margin[1] + self.offset[1]
                    w.width = w.minimum_width#offset_x - 2*self.margin[0]
                    #w.height = self.height
                    pos_x+= w.minimum_width + self.spacing
                    w.draw()
                elif isinstance(w, Spacer) :
                    #print(dir(w))
                    if w.minimum_width :
                        pos_x += w.minimum_width
                    else :
                        pos_x+= spacer_expand

        elif self.direction == 'VERTICAL' :
            if self.expand :
                self.width = self.region_size[0]
                #self.height = self.region_size[1]

            if not self.widgets :
                return

            widgets_height = sum([w.minimum_height for w in self.widgets])

            #offset_x = (self.width-2*self.margin[0]) / self.widgets

            #print(widgets_height)
            spacers = [w for w in self.widgets if isinstance(w,Spacer) and not w.minimum_height]

            #print(spacers)
            #print(len(spacers))

            free_space = (self.height-2*self.margin[1]-spacing) - widgets_height

            if self.expand :
                pass
                #print("height",self.height)
                #print("free_space",free_space)
                #print("widgets_height",widgets_height)

            if spacers :
                spacer_expand = free_space/len(spacers)
            #widget_len =

            #print(self.pos_x)

            pos_y = self.margin[1]/2
            for w in self.widgets :
                if isinstance(w,  (Widget,Property)) :
                    #print(w)
                    if w.hide :
                        continue

                    w.pos_x = self.pos_x +self.margin[0]/2+ self.offset[0]
                    w.pos_y = self.pos_y - pos_y + self.offset[1]
                    w.height = w.minimum_height#offset_x - 2*self.margin[0]
                    w.width = self.width - self.margin[0]
                    pos_y+= w.minimum_height + self.spacing
                    w.draw()
                else :
                    #print(dir(w))
                    if w.minimum_height :
                        pos_y += w.minimum_height
                    else :
                        pos_y+= spacer_expand
