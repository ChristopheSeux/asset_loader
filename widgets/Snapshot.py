from .Widget import Widget
from .Image import Image
from ..functions import *

class Snapshot(Widget):

    color = (0.5,0.5,0.5,0.5)
    border_color = (1,0.66,0.25,1)
    border_width = 1
    start = False

    mouse_start = ()
    mouse_window_start = ()

    def __init__(self,parent = None) :
        self.parent = parent

        self.parent.widgets.append(self)


    def draw(self) :
        space_data = bpy.context.space_data

        if self.left_mouse.press and not self.mouse_start:

            print("Start Snapshot")
            self.mouse_start = self.mouse
            self.mouse_window_start = self.mouse_window

            self.show_only_render = space_data.show_only_render
            self.show_manipulator = space_data.show_manipulator

            space_data.show_manipulator = False
            space_data.show_only_render = True

        elif self.mouse_start and self.left_mouse.release :
            print("End Snapshot")
            self.parent.widgets.remove(self)

            size = self.width-1

            buffer = Buffer(GL_BYTE, size * size * 4)
            glReadBuffer(GL_BACK)

            x = int(self.mouse_window_start[0]) +1
            y = int(self.mouse_window_start[1] - size)

            glReadPixels(x,y, size, size, GL_RGBA, GL_UNSIGNED_BYTE, buffer)

            snapshot_pixels = np.asarray(buffer, dtype=np.uint8)

            #print(snapshot_pixels)
            #.reshape(size,size*4)
            #snapshot_pixels = snapshot_pixels.astype(float)
            #snapshot_pixels = snapshot_pixels.astype(float)
            snapshot_pixels = np.divide(snapshot_pixels,255.0)
            snapshot_pixels = snapshot_pixels.reshape(size,size*4)

            PicSnapshot = Image(snapshot_pixels,size = (140,140))
            #PicSnapshot.border_width = ()
            PicSnapshot.border_style = 'dashed'
            PicSnapshot.border_color = self.border_color

            self.parent.SnapshotLayout.widgets = [PicSnapshot]

            self.mouse_start = ()
            self.parent.blocking = False

            space_data.show_only_render = self.show_only_render
            space_data.show_manipulator = self.show_manipulator



        if not self.mouse_start : return

        self.width = int(abs(self.mouse[0]-self.mouse_start[0]))
        self.height = int(abs(self.mouse[1]-self.mouse_start[1]))

        coords = [self.mouse_start,
                (self.mouse[0],self.mouse_start[1]),
                (self.mouse[0],self.mouse_start[1]-self.width),
                (self.mouse_start[0],self.mouse_start[1]-self.width)]

        glEnable(GL_BLEND)
        glEnable(GL_LINE_STIPPLE)

        # Draw square
        draw_square(*coords,self.border_color,GL_LINE_STRIP)

        glDisable(GL_BLEND)
        glDisable(GL_LINE_STIPPLE)
