import sys
import bpy,os
import json
from os.path import join,dirname,realpath,basename,normpath,splitdrive,exists
from glob import glob


def scantree(path):
    from os import scandir
    """Recursively yield DirEntry objects for given directory."""
    for entry in scandir(path):
        if entry.is_dir(follow_symlinks=False):
            yield from scantree(entry.path)  # see below for Python 2.x
        else:
            yield entry


args = eval(sys.argv[-1])



file_info = args['files_info']
store = args['store']
lib_path = args['lib_path']

replace_json = args['replace_json']
replace_image = args['replace_image']







#print("file_info",file_info)
#print("store",store)
#print("lib_path",lib_path)

scene = bpy.context.scene

areas = [a for a in bpy.context.screen.areas if a.type == 'VIEW_3D']
viewport = max(areas ,key = lambda x : x.width*x.height)


override = {'area': viewport, 'region': viewport.regions[-1]}



space_data = viewport.spaces[0]
space_data.region_3d.view_perspective = 'CAMERA'

# render preview
scene.render.resolution_x = 140
scene.render.resolution_y = 140
scene.render.antialiasing_samples = '16'
scene.render.use_full_sample = True
scene.render.resolution_percentage =100

space_data.show_only_render = True
space_data.viewport_shade = 'MATERIAL'
space_data.fx_settings.use_ssao = True

bpy.context.scene.render.alpha_mode = 'TRANSPARENT'


#set_display('solid')

sensor = scene.camera.data.sensor_width



for info in file_info :
    glob_path = info['path']

    paths = sorted(glob(glob_path))
    if not paths :
        print("%s not found"%info["path"])
        continue


    blend_path = paths[-1]
    group_name = info['asset']

    folder_name = basename(dirname(blend_path))
    asset_folder = join(lib_path,info['entity'],folder_name,group_name)


    #search already existing asset :
    json_files = [f.path for f in scantree(lib_path) if basename(f.path) == group_name+'.json']
    if json_files :
        if replace_json:
            asset_folder = dirname(json_files[0])
        else :
            continue


    if not os.path.exists(asset_folder):
        os.makedirs(asset_folder)


    if not replace_image and exists(join(asset_folder,group_name+'.png')) :
        continue

    #link Group
    with bpy.data.libraries.load(blend_path, link=True) as (data_src, data_dst):
        data_dst.groups = [group_name]

    g = data_dst.groups[0]

    if not g :
        continue

    empty = bpy.data.objects.new(group_name,None)
    scene.objects.link(empty)

    empty.dupli_type ='GROUP'
    empty.dupli_group = g

    empty.select = True

    bpy.ops.view3d.camera_to_view_selected(override)
    #scene.camera.location[1] -=0.4
    scene.camera.data.sensor_width *=1.1






    image_path = os.path.join(asset_folder,'%s.png'%group_name)
    scene.render.filepath = image_path

    bpy.ops.render.opengl(write_still = True)


    bpy.data.objects.remove(empty,True)
    bpy.data.groups.remove(g,True)

    scene.camera.data.sensor_width = sensor
    #bpy.ops.wm.revert_mainfile()



    asset_info={
        "type" : "groups",
        "asset" : group_name,
        "image" : "./%s.png"%(group_name),
        "tags" : "",
        "path": glob_path.replace(store,"{STORE}"),
        "description" : "",
        "boundbox" : [],
        "bg_color" : (0.4, 0.4, 0.4, 0.85),
                }

    json_path = join(asset_folder,group_name+'.json')
    with open(json_path, 'w') as outfile:
        json.dump(asset_info, outfile)


bpy.ops.wm.quit_blender()
