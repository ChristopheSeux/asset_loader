from .Property import Property
from .Widget import Widget
from .Picture import Picture
from ..functions import *

class Slider(Property):


    def __init__(self,data,property,text=None):
        self.state = 'DEFAULT'

        self.data= data
        self.property = property

        self.properties = self.data.bl_rna.properties[self.property]

        self.min = self.properties.soft_min
        self.max = self.properties.soft_max
        self.unit = self.properties.unit

        self.range = self.max - self.min


        self.unit = self.get_unit(self.properties.subtype)


        self.value = getattr(data,property)

        if text is None:
            self.text = text
            self.setText(self.property.replace('_',' ').title()+':')
        else :
            self.text = text
            self.setText(text+':')


    def processEvent(self) :
        self.isMouseOver()
        self.boundBox()

        if self.is_over  :
            if self.event_type == 'LEFTMOUSE' and self.event_value =='PRESS':
                self.active_value = self.value
                self.mouse_on_press = self.mouse
                self.is_pressed = True

        if self.event_value =='RELEASE':
            self.is_pressed = False


        if self.event_type =='MOUSEMOVE' and self.is_pressed :
            new_value = self.active_value+ (self.mouse[0]-self.mouse_on_press[0])/self.width*self.range

            if new_value > self.max :
                new_value = self.max
            if new_value < self.min :
                new_value = self.min

            setattr(self.data,self.property,new_value)

            self.run_action()

    def get_unit(self,unit) :
        unit_conversion = {'PIXEL' : 'px','PERCENTAGE' : '%','DEGREES' : '°'}
        if unit in unit_conversion :
            return unit_conversion[unit]
        else :
            return ''


    def setStyle(self,style) :
        for arr,value in style :
            setattr(self,attr,value)

    '''
    def set_value(self,event) :
        numbers = {"ZERO":0,"ONE":1,"TWO":2,"THREE":3,"FOUR":4,"FIVE":5,"SIX":6,"SEVEN":7,"HEIGHT":8,"NINE":9}

        if event.value in  numbers :
            self.value.append(numbers(event.value))

        if event.value in numbers.keys+["NUMPAD_"+str(i) for i in range(0,10)] :
            self.value.append(float(event.value))
    '''



    def draw(self) :

        self.processEvent()
        self.value = getattr(self.data,self.property)
        #self.value_label = str(getattr(self.data,self.property))
        #print(self.modal.event_value)




        glEnable(GL_BLEND)


        box = self.bound_box

        #rim
        glColor4f

        #print(upper_left,self.rim_offset)
        offset_rim_vector = Vector((self.rim_offset,-self.rim_offset))

        draw_square(*(box+offset_rim_vector),(1, 1, 1, 0.15))
        #draw_square(upper_left,upper_right,lower_right,lower_left)

        #background default color
        bg_color_top = (0.6, 0.6, 0.6, 1)
        bg_color_bottom = (0.68, 0.68, 0.68, 1)

        #Background mouse hover
        if self.is_pressed :
            bg_color_bottom = self.changeColor(bg_color_bottom,self.dark_offset)
            bg_color_top= self.changeColor(bg_color_top,self.dark_offset)

        elif self.is_over:
            bg_color_top = self.changeColor(bg_color_top,self.bright_offset)
            bg_color_bottom = self.changeColor(bg_color_bottom,self.bright_offset)


        draw_gradient(*box,bg_color_top,bg_color_bottom)



        bg_color_top = (0.5, 0.5, 0.5, 1)
        bg_color_bottom = (0.42, 0.42, 0.42, 1)

        #Background mouse hover
        if self.is_pressed :
            bg_color_bottom = self.changeColor(bg_color_bottom,self.dark_offset)
            bg_color_top= self.changeColor(bg_color_top,self.dark_offset)

        elif self.is_over:
            bg_color_top = self.changeColor(bg_color_top,self.bright_offset)
            bg_color_bottom = self.changeColor(bg_color_bottom,self.bright_offset)

        #slider value
        p2 = box[0]+Vector((self.width*self.value/self.range,0))
        p3 = box[3]+Vector((self.width*self.value/self.range,0))

        draw_gradient(box[0],p2,p3,box[3],bg_color_top,bg_color_bottom)


        #contour
        draw_square(*box,(0.2, 0.2, 0.2, 1),GL_LINE_STRIP)



        if self.is_pressed :
            glColor3f(0.95,0.95,0.95)
        else :
            glColor3f(0.05,0.05,0.05)



        # draw slider name
        blf.position(self.font_id, *box[3]+Vector((self.margin[0],self.margin[1])), 0)
        blf.size(self.font_id, 12, 72)
        blf.draw(self.font_id, self.text)

        # draw value
        value_txt = str(self.value)+self.unit

        dim = self.getTextSize(value_txt)
        blf.position(self.font_id, *box[2] +Vector((-self.margin[0]-dim[0],self.margin[1])),0)
        blf.size(self.font_id, 12, 72)
        blf.draw(self.font_id,value_txt)

        # restore opengl defaults
        glLineWidth(1)
        glDisable(GL_BLEND)
        glColor4f(0.0, 0.0, 0.0, 1.0)
