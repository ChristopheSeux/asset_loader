import bpy,json,os
#from rigutils import rig_info,find_fcurve_path
#from rigutils.insert_keyframe import insert_keyframe
from .func_layers import *
from mathutils import Matrix,Vector
from math import sqrt
from .utils import *
#from .utils import get_rotation



def load_data_block(path,asset_type,asset,link=True) :
    with bpy.data.libraries.load(path, link=link) as (data_src, data_dst):
        #all_groups = [name for name in data_src.groups]
        setattr(data_dst,asset_type,[asset])

    if not data_dst :
        print('The datablock %s is not found'%asset)
        return

    return getattr(data_dst,asset_type)[0]


def load_pose_3d(pose_data,ob=None,blend = 100,left = True,right = True,selected_only=True,frame = None,insert_keyframe = True) :
    if not ob :
        ob = bpy.context.object
        if not ob :
            print('No active object')
            return

    selected_bones = [b for b in ob.pose.bones if b.bone.select]

    if not ob.animation_data :
        ob.animation_data_create()

    if not ob.animation_data.action :
        ob.animation_data.action = bpy.data.actions.new(ob.name+'_action')

    #print("load_pose_3d_frame",frame)
    if frame == None :
        frame = bpy.context.scene.frame_current

    for bone_name, channel_info in pose_data.items() :
        bone = ob.pose.bones.get(bone_name)

        if not bone : continue

        if selected_only and not bone.bone.select and selected_bones : continue

        if not left and get_name_side(bone_name) == 'LEFT':
            print(bone_name)
            continue

        if not right and get_name_side(bone_name) == 'RIGHT':
            print(bone_name)
            continue

        #transforms
        if channel_info.get('transforms') :
            for channel_name, keyframe_info in channel_info['transforms'].items() :
                for index,value in keyframe_info.items() :
                    index = int(index)
                    channel = getattr(bone,channel_name)
                    current_value = channel[index]

                    new_value = current_value+(value - current_value)*blend*0.01

                    channel[index] = new_value

                    if insert_keyframe :
                        data_path = 'pose.bones["%s"].%s'%(bone_name,channel_name)
                        add_keyframe(ob,data_path,new_value,bone_name,frame = frame,index =index )



        # props
        if channel_info.get('props') :
            for channel_name,value in channel_info['props'].items() :

                if not left and get_name_side(channel_name) == 'LEFT':
                    print(channel_name)
                    continue

                if not right and get_name_side(channel_name) == 'RIGHT': continue

                if channel_name in bone.keys() :
                    current_value = bone[channel_name]
                    new_value = current_value+(value - current_value)*blend*0.01

                    if isinstance(current_value, int) :
                        print(channel_name)
                        new_value = round(new_value)

                    bone[channel_name] = new_value

                    if insert_keyframe :
                        data_path = 'pose.bones["%s"]["%s"]'%(bone_name,channel_name)
                        add_keyframe(ob,data_path,new_value,bone_name,frame = frame)


def create_node(jsonNode):
    node_tree = bpy.context.scene.node_tree
    newNode = node_tree.nodes.new(type=jsonNode['bl_idname'])

    for key,value in jsonNode.items():

        if key == 'node_tree':
            group = value['name']
            library = value['library']
            node_group = load_data_block(library,'node_groups',group,True)

            newNode.node_tree = node_group

        elif key == 'scene':
            newNode.scene = bpy.context.scene
        elif key == 'file_slots':
            for jsonIndex,jsonFs in enumerate(jsonNode[key]):
                if jsonIndex == 0:
                    fs = newNode.file_slots[0]
                    fs.path = jsonFs['path']
                else:
                    fs = newNode.file_slots.new(jsonFs['path'])
                newNode.inputs[jsonIndex].name = jsonNode['inputs'][jsonIndex]['identifier']
        else:
            try:
                print(newNode,key,value)
                setattr(newNode,key,value)
                #exec('newNode.%s = value'%key)
            except AttributeError:
                pass

    for key,value in jsonNode.items():
        if key == 'inputs':
            for jsonIndex,jsonInput in enumerate(jsonNode[key]):
                for inputKey,inputValue in jsonInput.items():
                    try:
                        exec('newNode.inputs[%s].%s = %s'%(jsonIndex,inputKey,inputValue))
                    except (NameError,AttributeError,SyntaxError,TypeError,ValueError):
                        pass

    return newNode

def load_compo_tree(jsonFile,Replace=False):
    scene = bpy.context.scene
    node_tree = bpy.context.scene.node_tree

    def create_link(jsonLink):
        nodeFrom = node_tree.nodes[jsonLink['from_node']['name']]
        nodeTo = node_tree.nodes[jsonLink['to_node']['name']]

        socketStart = nodeFrom.outputs[jsonLink['from_socket']['index']]
        socketEnd = nodeTo.inputs[jsonLink['to_socket']['index']]

        newLink = node_tree.links.new(input=socketStart,output=socketEnd,verify_limits=True)


    f = open(jsonFile,'r')
    jsonData = json.loads(f.read())
    f.close()

    scene.use_nodes = True

    if Replace:
        for node in node_tree.nodes:
            node_tree.nodes.remove(node)

    nodes = []
    name_convert = {}
    for key,value in jsonData.items():
        if key == 'nodes':
            for nodeKey,nodeValue in value.items():
                node = create_node(nodeValue)

                name_convert[nodeKey] = node.name
                nodes.append(node)


    for key,value in jsonData.items():
        if key == 'links':
            for linkKey,linkValue in value.items():
                linkValue['from_node']["name"] = name_convert[linkValue['from_node']["name"]]
                linkValue['to_node']["name"] = name_convert[linkValue['to_node']["name"]]
                #linkKey = name_convert[linkValue['name']]
                create_link(linkValue)

    return nodes

def load_brush(brushdic):
    '''Get brush dic and create construct brush datablock'''
    newb = bpy.context.scene.tool_settings.gpencil_brushes.new(brushdic['name'])#+'-test'
    for attr in ['random_subdiv', 'angle_factor', 'line_width', 'strength',
    'angle', 'use_random_pressure', 'random_press', 'use_jitter_pressure',
    'use_strength_pressure', 'jitter', 'pen_sensitivity_factor', 'pen_subdivision_steps',
    'pen_smooth_factor', 'pen_smooth_steps', 'use_pressure', 'use_random_strength']:
        setattr(newb, attr, brushdic[attr])

    for curve_type in ('curve_jitter', 'curve_sensitivity', 'curve_strength'):
        mapcurve = getattr(newb, curve_type)
        for curveprop in brushdic[curve_type]:
            print('setting', curveprop)
            #mapcurve = getattr(mapcurve_ob, curveprop)
            if curveprop == 'curves':
                print('>>>', brushdic[curve_type]['curves'])
                for i, points in enumerate(brushdic[curve_type]['curves'][0]):
                    print('points', points)
                    if i < 2:
                        print(mapcurve)
                        print(mapcurve.curves[0])
                        print(mapcurve.curves[0].points[0])
                        mapcurve.curves[0].points[i].location = points['location']
                        mapcurve.curves[0].points[i].handle_type = points['handle_type']
                        mapcurve.curves[0].points[i].select = points['select']
                    else:
                        #create new
                        newpoint = mapcurve.curves[0].points.new(points['location'][0],points['location'][1])
                        setattr(newpoint, 'handle_type', points['handle_type'])
                        setattr(newpoint, 'select', points['select'])
                    mapcurve.update()
            else:
                setattr(mapcurve, curveprop, brushdic[curve_type][curveprop])



def find_fcurve_path(ob,boneId,path, array_index) :
    try :
        dstChannel = eval('ob.pose.bones["%s"].%s[%d]'%(boneId,path,array_index))
        dstChannelStr = 'ob.pose.bones["%s"].%s[%d]'%(boneId,path,array_index)
        data_path = 'pose.bones["%s"].%s'%(boneId,path)

    except SyntaxError:
        try :
            dstChannel = eval('ob.pose.bones["%s"]%s'%(boneId,path))
            dstChannelStr = 'ob.pose.bones["%s"]%s'%(boneId,path)
            data_path = 'pose.bones["%s"]%s'%(boneId,path)
        except KeyError :
            dstChannel = None
            dstChannelStr = None
            data_path = None

    except :
        try :
            dstChannel = eval('ob.pose.bones["%s"].%s'%(boneId,path))
            dstChannelStr = 'ob.pose.bones["%s"].%s'%(boneId,path)
            data_path = 'pose.bones["%s"].%s'%(boneId,path)

        except KeyError :
            dstChannel = None
            dstChannelStr = None
            data_path = None

    return dstChannel,dstChannelStr,data_path




def load_anim_3d(action,blend,left,right,selected_only,mirror,frame = None):
    scene = bpy.context.scene
    ob = scene.objects.active

    #print("blend",blend)

    if not frame :
        frame = scene.frame_current

    selected_bones = [bone.name for bone in ob.pose.bones if bone.bone.select==True]

    if not len(selected_bones) or not selected_only:
        selected_bones =[bone.name for bone in ob.pose.bones]

    exclude_suffixe = []
    if not left :
        exclude_suffixe.append('.L')
    if not right :
        exclude_suffixe.append('.R')

    exclude_suffixe = tuple(exclude_suffixe)

    for fcurve,value in action.items():
        if mirror :
            mirrored_fcurve = find_mirror(fcurve)
            if find_mirror(fcurve) :
                fcurve = mirrored_fcurve

        bone = ob.pose.bones.get(fcurve)
        if bone :
            for path,channel in value.items() :
                exclude_fc = False
                if path.endswith(exclude_suffixe) or fcurve.endswith(exclude_suffixe) or fcurve not in selected_bones:
                    print(fcurve)
                    exclude_fc = True

                if mirror :
                    mirrored_path = find_mirror(path)
                    if mirrored_path :
                        path = mirrored_path


                for array_index,attributes in channel.items() :
                    array_index = int(array_index)

                    correct_path = find_fcurve_path(ob,fcurve,path, array_index)
                    dstChannel = correct_path[0]
                    dstChannelStr = correct_path[1]
                    data_path = correct_path[2]


                    for keysetting in attributes :
                        if mirror  :
                            if len(keysetting)<3 :
                                keysetting = mirror_key(path,array_index,keysetting,True)
                            else :
                                keysetting = mirror_key(path,array_index,keysetting,False)

                        dict = {
                            'interpolation' :       keysetting[0],
                            'type' :                keysetting[2],
                                }

                        if len(keysetting)<3 :
                            dict['handle_left']=        [keysetting[3][0]+frame,keysetting[3][1]]
                            dict['handle_left_type']=   keysetting[4]
                            dict['handle_right']=       [keysetting[5][0]+frame,keysetting[5][1]]
                            dict['handle_right_type']=  keysetting[6]
                            dict['easing']=             keysetting[7]
                            dict['back']=               keysetting[8]
                            dict['amplitude']=          keysetting[9]
                            dict['period']=             keysetting[10]

                        #if exclude_fc :
                        #    newValue = keysetting[1][2]
                        #else :
                            #print('###')
                            #print(keysetting[1][1],keysetting[1][2])
                        #    newValue = keysetting[1][2]+(keysetting[1][1]-keysetting[1][2])*blend


                        if dstChannelStr:
                            #  KEYFRAME CREATION
                            if not exclude_fc :
                                if not ob.animation_data:
                                    ob.animation_data_create()

                                if not ob.animation_data.action :
                                    action = bpy.data.actions.new(ob.name)
                                    ob.animation_data.action = action

                                fcurves = ob.animation_data.action.fcurves

                                fc = fcurves.find(data_path,array_index)


                                if not fc :
                                    group = ob.animation_data.action.groups.get(fcurve)
                                    oldValue = ob.path_resolve(data_path)

                                    if hasattr(oldValue,'__getitem__') :
                                        oldValue = oldValue[array_index]
                                    fc = fcurves.new(data_path,array_index,fcurve)

                                else :
                                    oldValue = fc.evaluate(keysetting[1][0]+frame)

                                if not fc.group and fcurve not in ob.animation_data.action.groups.keys():
                                    group = ob.animation_data.action.groups.new(fcurve)
                                    fc.group = group
                                #print(attributes['co'][0])

                                print("oldValue",oldValue)
                                newValue = oldValue+(keysetting[1][1] - oldValue)*blend
                                print("newValue",newValue)

                                keyframe = fc.keyframe_points.insert(frame=int(keysetting[1][0])+frame, value=newValue)

                                for key,val in dict.items() :
                                    setattr(keyframe,key,val)

                                if type(dstChannel) == int:
                                    exec('%s = %s'%(dstChannelStr,int(newValue)))
                                else :
                                    exec('%s = %s'%(dstChannelStr,float(newValue)))

    if ob.proxy_group :
        for o in ob.proxy_group.dupli_group.objects :
            if o.type == 'MESH' :
                o.data.update()





def load_grease_pencil(ob,dst_gp,gpencil_data,replace=True) :
    scene = bpy.context.scene

    selected_bones = []
    if ob.type =='ARMATURE' :
        selected_bones = [b for b in ob.pose.bones if b.bone.select]

    # create missing palette
    palette = dst_gp.palettes.active
    if not palette :
        palette = dst_gp.palettes.new("palette")

    for p_name,p_value in gpencil_data["palettes"].items():
        if not palette.colors.get(p_name) :
            p = palette.colors.new()
            p.name = p_name
            for attr in ['color','alpha','fill_color','fill_alpha'] :
                setattr(p,attr,p_value[attr])

                '''
    # removing unexisting layer and frames
    for layer in dst_gp.layers :
        match_source = [l for l in gpencil_data["layers"] if layer.info.endswith(l["info"])]
        if match_source :
            if not match_source[0]["frames"] :
                layer.frames.remove(layer.active_frame)
                print("remove gp frame")
        else :
            dst_gp.layers.remove(layer)
            print("remove layer")
            '''


    for src_layer in gpencil_data["layers"] :
        #layer_name = src_layer['info']
        namespace = ob.name.split('_',1)[0]
        if ob.proxy_group :
            namespace = ob.proxy_group.name.split('_',1)[0]
        layer_name = '%s_%s'%(namespace,src_layer['info'])

        #Don't create layer
        if selected_bones :
            bone = ob.pose.bones.get(src_layer["parent_bone"])
            if bone and not bone.bone.select :
                continue


        dst_layer = dst_gp.layers.get(layer_name)
        if not dst_layer:
            dst_layer = dst_gp.layers.new(layer_name)

        dst_layer.parent = ob
        for attr in ["hide","line_change","opacity","show_x_ray"] :
            setattr(dst_layer,attr,src_layer[attr])

        try :
            dst_layer.parent_type = src_layer['parent_type']
            if src_layer["parent_type"]=='BONE' :
                dst_layer.parent_bone = src_layer['parent_bone']
                dst_layer.matrix_inverse = Matrix(src_layer['matrix_inverse'])

        except TypeError :
            print("Could not parent to %s"%src_layer['parent_bone'])

        if src_layer["parent"] and src_layer["parent_type"] == 'OBJECT':
            dst_layer.matrix_inverse = Matrix(src_layer['matrix_inverse'])

        '''
        frames={}
        for i,f in enumerate(dst_layer.frames) :
            frames[f.frame_number]=i

        if not scene.frame_current in frames.keys():
            dst_frame = dst_layer.frames.new(scene.frame_current)
        else :
            dst_frame = dst_layer.frames[frames[scene.frame_current]]

            if replace :
                dst_frame.clear()
                '''
        if src_layer.get('strokes') :  #old_storage_system
            src_frames = [{"frame_number" : 0,"strokes":src_layer["strokes"]}]

        elif src_layer.get('frames') :
            src_frames = src_layer["frames"]

        for src_frame in src_frames :
            src_frame_number = src_frame['frame_number'] + scene.frame_current
            dst_frame = [f for f in dst_layer.frames if f.frame_number == src_frame_number]

            if dst_frame  :
                dst_frame = dst_frame[0]
            else :
                dst_frame = dst_layer.frames.new(src_frame_number)

            if replace :
                dst_frame.clear()

            # create strokes
            for src_stroke in src_frame["strokes"]:
                dst_stroke = dst_frame.strokes.new(src_stroke["palette"])

                for attr in ["line_width","draw_mode"] :
                    setattr(dst_stroke,attr,src_stroke[attr])

                for i,src_point_values in enumerate(src_stroke["points"]):
                    dst_stroke.points.add()
                    dst_point = dst_stroke.points[i]

                    for attr in ["co","pressure","strength"] :
                        setattr(dst_point,attr,src_point_values[attr])




def load_group(blend_path,group_name,instantiate = True, make_proxy = True,link=True) :
    scene = bpy.context.scene

    if not os.path.exists(blend_path) :
        print("The path %s not exist"%blend_path)
        return

    with bpy.data.libraries.load(blend_path, link=link) as (data_src, data_dst):
        #all_groups = [name for name in data_src.groups]
        data_dst.groups = [group_name]

    group = data_dst.groups[0]

    proxy = None
    empty = None

    if instantiate :
        if link :
            empty = bpy.data.objects.new(group_name,None)
            scene.objects.link(empty)

            empty.dupli_type ='GROUP'
            empty.dupli_group = group


            armatures = [o.find_armature() for o in group.objects if o.find_armature()]
            armatures += [o.parent for o in group.objects if o.parent and o.parent.type == 'ARMATURE']
            if make_proxy and armatures :
                rig = group.objects.get(group.name+'_rig')
                if not rig :
                    rig = max(set(armatures), key=armatures.count)

                scene.objects.active = empty
                override = bpy.context.copy()
                override['object'] =empty
                override['active_object'] = empty

                bpy.ops.object.proxy_make(override,object=rig.name)

                proxy = scene.objects.active

                proxy.lock_location = [True,True,True]
                proxy.lock_rotation = [True,True,True]
                proxy.lock_scale = [True,True,True]

                empty.lock_location = [True,True,True]
                empty.lock_rotation = [True,True,True]
                empty.lock_scale = [True,True,True]


            # empty draw size
            bound_boxes = [[o.matrix_world*Vector(v) for v in o.bound_box] for o in empty.dupli_group.objects]
            x_min = sorted([b[0][0] for b in bound_boxes])
            x_max = sorted([b[4][0] for b in bound_boxes])

            y_min = sorted([b[0][1] for b in bound_boxes])
            y_max = sorted([b[3][1] for b in bound_boxes])

            x_size = abs(x_max[-1]-x_min[0])
            y_size = abs(y_max[-1]-y_min[0])

            bound_box_size = (x_size+y_size)/2
            if bound_box_size < 1 :
                empty.empty_draw_size =  sqrt(bound_box_size)

        else :
            ob_layers =get_objects_layers(group.objects)

            for layer_index, objects in ob_layers.items() :
                if layer_index in get_used_layers():
                    layer_index = get_free_layers()[0]

                for ob in objects :
                    ob = scene.objects.link(ob)
                    ob.layers = layers_from_indexes([layer_index])
    else :
        print(group_name,"n'est pas dans le blend")


    return group,empty,proxy

def update_lib(lib,new_path) :
    scene = bpy.context.scene

    #groups = [g for g in lib.users_id if type(g)==bpy.types.Group]

    #[item for sublist in l for item in sublist]
    #objects = [o for g in groups for o in g.objects] #
    #proxies = [o for o in scene.objects if o.proxy in objects]


    rigs = [o for o in scene.objects if o.type == 'ARMATURE' and o.proxy and o.proxy.library == lib]

    rigs_info = {}
    for rig in rigs :
        print('rig',rig,rig.proxy)
        new_rig = load_data_block(new_path,"objects",rig.proxy.name,link=True)

        rigs_info[rig] = rig_info(new_rig)

        bpy.data.objects.remove(new_rig)


    lib.filepath = new_path
    lib.reload()

    scene.update()

    for rig,info in rigs_info.items() :
        for bone in [b for b in rig.pose.bones if b.name in info]:
            bone_info = info.get(bone.name)
            bone.rotation_mode = bone_info['rotation_mode']
            bone.bone_group = rig.pose.bone_groups.get(bone_info['bone_group'])


    '''
    else :
        for group in groups :
            proxies = [o for o in scene.objects if o.proxy in list(group.objects)]

            old_libs = list(bpy.data.libraries)

            new_group = load_group(new_path,group.name,instantiate = False)[0]
            new_libs = list(bpy.data.libraries)
            print('######')
            print(old_libs)
            print('######')
            print(new_libs)

            proxies_info = {}

            for proxy in [o for o in scene.objects if o.proxy in list(group.objects)] :
                old_rig = proxy.proxy
                new_rig = new_group.objects.get(old_rig.name)
                proxies_info[new_rig] = rig_info(new_rig)

            bpy.data.groups.remove(new_group,True)


            lib.filepath = new_path
            lib.reload()

            bpy.context.scene.update()

            for lib in [l for l in new_libs if l not in old_libs] :
                lib.filepath = ''

            for proxy,info in proxies_info.items() :
                for bone_name,bone_info in info.items() :
                    bone = proxy.pose.bones.get(bone_name)

                    if bone :
                        bone.rotation_mode = bone_info['rotation_mode']

                        bone.bone_group = proxy.pose.bone_groups.get(bone_info['bone_group'])

                        #bone.bone_group = proxy.pose.bone_groups[2]

                        for c_name,c_value in bone_info['constraints'].items() :
                            if not bone.constraints.get(c_name) :
                                new_constraint = bone.constraints.new(c_value['type'])

                                for key,value in c_value.items() :
                                    try :
                                        setattr(new_constraint,key,value)
                                    except :
                                        pass
                                        '''
