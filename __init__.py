bl_info = {
    "name": "Asset Loader",
    "author": "Christophe Seux",
    "version": (0, 1),
    "blender": (2, 79, 0),
    "category": "User"}

if "bpy" in locals():
    import importlib
    importlib.reload(operator_modal)
    importlib.reload(operators)
    importlib.reload(functions)
    importlib.reload(properties)

from .import operator_modal
from .import operators
from .import properties
from .properties import AssetLoaderSettings

import bpy

def draw_button(self,context) :
    layout = self.layout
    layout.operator("view3d.asset_loader",text="",icon = 'SCENE_DATA')


def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.asset_loader = bpy.props.PointerProperty(type= AssetLoaderSettings)

    bpy.types.VIEW3D_HT_header.append(draw_button)


def unregister():
    bpy.utils.register_module(__name__)
    del bpy.types.Scene.asset_loader

    bpy.types.VIEW3D_HT_header.remove(draw_button)
