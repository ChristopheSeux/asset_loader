from .utils import *

def bevel_corner(p,p1,p2,segments,offset,relative=False) :
    from mathutils import Vector
    from math import cos,sin,pi,atan2
    from mathutils.geometry import intersect_line_line_2d as intersect_line
    from mathutils.geometry import intersect_point_line

    vec1 = p1-p
    vec2 = p2-p

    up_vector = Vector((0,1))
    right_vector = Vector((1,0))

    normal1 = Vector((vec1[1],-vec1[0]))
    if normal1.dot(vec2) <= 0 :
        normal1 = -normal1

    normal2 = Vector((-vec2[1],vec2[0]))
    if normal2.dot(vec1) <= 0 :
        normal2 = -normal2

    def find_center(offset) :
        offset_p1 = p + vec1.normalized()*offset
        offset_p2 = p + vec2.normalized()*offset

        offset_vec1 = normal1.normalized()*100
        offset_vec2 = normal2.normalized()*100

        center = intersect_line(offset_p1,offset_p1+offset_vec1,offset_p2,offset_p2+offset_vec2)

        return center,offset_p1,offset_p2

    max_offset = min(vec1.length,vec2.length) /2


    if relative :
        if offset > 1 :
            offset = 1

        offset = max_offset/offset

        center,offset_p1,offset_p2 = find_center(offset)

    else :

        offset_vec1 = normal1.normalized()*offset
        offset_vec2 = normal2.normalized()*offset

        V1 = vec1.normalized()*100
        V2 = vec2.normalized()*100

        center = intersect_line((p-V1)+offset_vec1,(p1+V1)+offset_vec1,
                                (p-V2)+offset_vec2,(p2+V2)+offset_vec2)

        offset_p1 = intersect_point_line(center,p,p1)[0]
        offset_p2 = intersect_point_line(center,p,p2)[0]

        if (offset_p1-p).length > max_offset or (offset_p2-p).length > max_offset :
            center,offset_p1,offset_p2 = find_center(offset)


    r = (center - offset_p1).length

    start_angle = -(offset_p1-center).angle(right_vector)
    if (offset_p1-center).cross(right_vector) <= 0 :
        start_angle = -start_angle


    step_angle = (center-offset_p1).angle((center-offset_p2))/segments
    if (center-offset_p1).cross(center-offset_p2) <=0 :
        step_angle = -step_angle

    points = [[center[0]+r*cos(start_angle+step_angle*i),center[1]+r*sin(start_angle+step_angle*i)] for i in range(1,segments)]

    points.insert(0,offset_p1)
    points.append(offset_p2)

    return points


def draw_gradient(p1,p2,p3,p4,top_color=None,bottom_color = None) :
    glEnable(GL_BLEND)
    glBegin(GL_POLYGON)

    if not bottom_color :
        bottom_color = top_color

    if top_color:
        glColor4f(*top_color)
    glVertex2f(*p1)
    glVertex2f(*p2)

    if top_color:
        glColor4f(*bottom_color)
    glVertex2f(*p3)
    glVertex2f(*p4)

    if top_color:
        glColor4f(*top_color)
    glVertex2f(*p1)

    glEnd()

    glDisable(GL_BLEND)

def draw_square(p1,p2,p3,p4,color = (0.5,0.5,0.5,1),primitive = GL_POLYGON) :
    glEnable(GL_BLEND)
    glBegin(primitive)

    glColor4f(*color)

    glVertex2f(*p1)
    glVertex2f(*p2)
    glVertex2f(*p3)
    glVertex2f(*p4)
    glVertex2f(*p1)

    glEnd()

    glDisable(GL_BLEND)
