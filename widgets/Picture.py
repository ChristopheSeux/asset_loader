from ..functions import *
from threading import Thread
from PIL import Image

class Picture() :
    def __init__(self,data,size=(32,32)) :
        self.pos = Vector((0,0))
        self.size = size

        self.buffered = False
        self.texbuffer = None
        self.minimum_width = 32

        if isinstance(data, bpy.types.Image) :
            data.gl_load(GL_NEAREST, GL_NEAREST)
            self.texture_id = data.bindcode[0]
            self.data = None

        else :
            self.texbuffer = Buffer(GL_INT, [1])
            glGenTextures(1, self.texbuffer)
            self.texture_id = self.texbuffer[0]

            if isinstance(data,str) :
                self.path = data
                self.load_image()

            else :
                self.data = data
                self.path = ''
                self.img_size = (len(self.data),int(len(self.data[0])/4))

                print(self.img_size)
            #self.buffer_image()

    def load_image(self):
        img = Image.open(self.path)
        self.data = np.array(img)
        self.data = np.flipud(self.data)/255.0
        #self.data = self.data.astype(float)
        #self.data = np.divide(self.data,255.0)

        self.img_size = img.size
        self.channels = 4 if img.mode == "RGBA" else 3

        self.data  = self.data.reshape(img.size[1],img.size[0]*self.channels)
        #self.buf = Buffer(GL_BYTE, [len(content),len(content[0])], content)


    def buffer_image(self):
        if self.buffered: return
        glBindTexture(GL_TEXTURE_2D, self.texture_id)
        #glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        # texbuffer = Buffer(GL_BYTE, [self.width,self.height,self.depth], image_data)
        #image_size = self.size[0]*self.size[1]*self.channels
        glPixelStorei(GL_UNPACK_ALIGNMENT,1)

        if self.data is not None:
            self.texbuffer = Buffer(GL_FLOAT, [len(self.data),len(self.data[0])], self.data)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, self.img_size[0], self.img_size[1], 0, GL_RGBA, GL_FLOAT, self.texbuffer)
            del self.texbuffer
        glBindTexture(GL_TEXTURE_2D, 0)
        self.buffered = True

    def draw(self):
        self.buffer_image()

        p1 = self.pos
        p2 = self.pos + Vector((0,self.size[1]))
        p3 = self.pos + Vector((self.size[0],self.size[1]))
        p4 = self.pos + Vector((self.size[0],0))

        glColor4f(1,1,1,1)

        glEnable(GL_TEXTURE_2D)
        glEnable(GL_BLEND)


        glBindTexture(GL_TEXTURE_2D, self.texture_id)

        glBegin(GL_QUADS)
        glTexCoord2f(0,0)
        glVertex2f(*p1)
        glTexCoord2f(0,1)
        glVertex2f(*p2)
        glTexCoord2f(1,1)
        glVertex2f(*p3)
        glTexCoord2f(1,0)
        glVertex2f(*p4)
        glEnd()
        glDisable(GL_BLEND)
        glDisable(GL_TEXTURE_2D)
        #glBindTexture(GL_TEXTURE_2D, 0)
