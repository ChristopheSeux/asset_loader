from .Widget import Widget
from .Picture import Picture
from ..functions import *
from .Layout import Layout

class Menu(Widget):

    layout = None
    color = (0.05,0.05,0.05,0.75)

    width = 154
    height = 100

    def __init__(self,items) :
        self.items = items

        self.height = len(self.items)*24 +4
        self.layout = Layout(direction = 'VERTICAL',margin = (4,4))

        style = {
            "bg_color" : (0,0,0,0),
            "bg_select_color" :(0.33,0.5,0.753,1),
            "rim_color" : (0,0,0,0),
            "border_width" :  0
        }

        self.pos_x = self.mouse[0] - 15
        self.pos_y = self.mouse[1] + 10

        for item in self.items :
            item.setStyle(style)

            self.layout.addWidget(item)


    def processEvent(self) :

        #self.pos_x  = 50
        #self.width = self.region_size[0]
        #self.height = 100
        #self.pos_y = self.region_size[1]

        self.isMouseOver()
        self.boundBox()

    def setLayout(self,layout) :
        self.layout = layout
        #layout.width = self.width
        #layout.width = self.width

    def draw(self) :



        self.processEvent()



        #print(self.modal.mouse)
        #self.isMouseOver()

        draw_square(*self.bound_box,color =self.color)



        #glLineWidth(1)
        #glBegin(GL_LINE_LOOP)
        #draw_square(*self.bound_box)
        #glEnd()

        if self.layout :
            self.layout.width = self.width
            self.layout.height = self.height
            self.layout.pos_x = self.pos_x
            self.layout.pos_y = self.pos_y

            self.layout.draw()
