import bpy



def get_used_layers() :
    used_layers = []
    for ob in bpy.context.scene.objects :
        for i,l in enumerate(ob.layers) :
            if l and i not in used_layers :
                used_layers.append(i)

    return used_layers

def get_free_layers():
    free_layers=[]
    for i in range(19) :
        if i not in get_used_layers() :
            free_layers.append(i)

    return free_layers


def layers_from_indexes(indexes) :
    layers = [False]*20
    for index in indexes :
        layers[index] = True

    return layers

def get_ob_layers(ob) :
    layers =[]
    for i,l in enumerate(ob.layers) :
        if l and i not in layers :
            layers.append(i)

    return layers

def get_objects_layers(objects):
    Layers = {}

    for ob in objects :
        first_layer = get_ob_layers(ob)[0]
        if not Layers.get(first_layer):
            Layers[first_layer] = []

        Layers[first_layer].append(ob)

    return Layers
