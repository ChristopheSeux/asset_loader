from ..functions import *
from .Picture import Picture

class MouseButton() :
    press = False
    double_click = False
    click = False
    release = False
    down = False
    tmp_down = False
    any = True
    mouse_start = Vector((0,0))
    mouse_end = Vector((0,0))
    press_count = 0
    press_time = 0


class Widget():
    pos_x = 0
    pos_y = 0

    minimum_width = 0
    minimum_height = 0

    width = 200
    height = 23

    rim_offset = 1

    margin = (8,6)

    is_over = False
    is_active = False
    is_pressed = False

    bright_offset = 0.1
    dark_offset = -0.1

    font_id = 0  # XXX, need to find out how best to get this.

    bound_box = []
    icon = None

    modifiers = {}
    ascii = None

    event_type = None
    event_value = None

    left_mouse = MouseButton()
    right_mouse = MouseButton()
    middle_mouse = MouseButton()

    mouse = Vector((0,0))
    #mouse_start = Vector((0,0))
    #mouse_end = Vector((0,0))

    active_widgets = []

    style = {}

    hide = False

    modal = None
    area = None
    connect = None
    #double_click_connect = None
    #actions = {}
    #texture_path = None
    #texture = None

    #def SetRightClick() :
    #    if self.isMouseOver() and self.isKeyPress('RIGHTMOUSE') :
    #    menu = Menu()

    #def connect(self,mouse,click,func,args) :
    def run_action(self) :
        if self.connect :
            if isinstance(self.connect, (tuple,list)) :
                action,args = self.connect
                action(**args)
            else :
                self.connect()


    def setStyle(self,style) :
        self.style = {**self.style,**style}

        #for arr,value in style :
        #    setattr(self,attr,value)

    def changeColor(self,color,offset):
        rgb = Color(color[:3])
        rgb.v += offset*rgb.v
        alpha = color[3:]

        return tuple(rgb)+alpha


    def setMinimumSize(self,size = None) :
        if size is None :
            if self.icon and not self.text :
                self.minimum_width =  23
                return
            dim = 2*self.margin[0]
            text_dim = self.getTextSize(self.text)
            dim+= text_dim[0]

            if hasattr(self,"value") :
                print("value",self.value)

                space_dim = self.getTextSize(" ")
                value_dim = self.getTextSize(str("%.02f"%self.value))

                dim+=space_dim[0]
                dim+=value_dim[0]

            if self.icon :
                dim += 10

            self.minimum_width =  dim
        else :
            self.minimum_width =  size

    def setIcon(self,icon_name) :
        full_path = icon_name
        if not splitext(icon_name)[1] :
            full_path = icon_name+'.png'

        if basename(icon_name) == icon_name :
            full_path = join(dirname(__file__),icon_name)

        self.icon = Picture(full_path,size =(20,20))

        self.setMinimumSize()

    def setText(self,text) :
        self.text = text
        self.setMinimumSize()

    def getTextSize(self,text) :
        return blf.dimensions(self.font_id,text)

    def isKeyPress(self,key) :
        return self.event_type == key and self.event_value == 'PRESS'

    def isKeyRelease(self,key) :
        return self.event_type == key and self.event_value == 'RELEASE'

    def isMouseOver(self) :
        self.is_over = False

        if self.mouse[0] <=0 or self.mouse[1] <=0 :  return

        in_x = self.pos_x<self.mouse[0]<self.pos_x+self.width
        in_y = self.pos_y-self.height<self.mouse[1]<self.pos_y

        if in_x and in_y :
            self.is_over = True

    def boundBox(self) :
        upper_left = Vector((self.pos_x, self.pos_y))
        upper_right = Vector((self.pos_x+self.width, self.pos_y))
        lower_right = Vector((self.pos_x+self.width, self.pos_y-self.height))
        lower_left = Vector((self.pos_x, self.pos_y-self.height))

        self.bound_box = np.array((upper_left,upper_right,lower_right,lower_left))
