from .Widget import Widget
from .Picture import Picture
from .Layout import Layout
from .Frame import Frame
from ..functions import *
from time import time

class WidgetList(Widget):
    def __init__(self,items,direction = 'VERTICAL',padding = 5):

        self.direction = 'DIRECTION'
        self.select = False
        self.items = items

        self.padding = padding

        self.scroll_x = 0
        self.scroll_y = 0

        self.Frame = Frame(self)
        self.Frame.color = (0.25,0.25,0.25,0.85)
        self.Frame.minimum_height = 193

        self.minimum_height = 193


        self.Layout = Layout(direction = 'VERTICAL')

        self.context_menu = None


        #self.Frame.setLayout(self.Layout)

        self.Layout.addWidget(self.Frame)

        self.WidgetLayout = Layout(margin = (10,5),spacing = 5)
        self.Frame.setLayout(self.WidgetLayout)

        self.active_widget = None
        self.selected_widgets = []

        self.mouse_start = None
        #self.scroll_start = None
        self.is_scrolling = False

        self.context_menu_show = False
        self.context_menu_pop = 0
        self.context_menu_close = 0


        for item in self.items :
            self.WidgetLayout.addWidget(item)


    @property
    def selected_items(self):
        return [i for i in self.items if i.select]

    def setMinimumSize(self,size = None) :
        #return
        self.minimum_width = self.size[0]
        """
        if size is None :
            if self.icon and not self.text :
                self.minimum_width =  22
                return
            dim = 2*self.margin[0]
            text_dim = self.getTextSize(self.text)
            dim+= text_dim[0]

            if self.icon :
                dim += 10

            self.minimum_width =  dim
        else :
            self.minimum_width =  size
            """


    def reset_scroll(self) :
        self.WidgetLayout.offset = Vector((0,0))

    def processEvent(self) :
        self.isMouseOver()
        self.boundBox()

        self.Layout.width = self.width
        self.Layout.height = self.height
        self.Layout.pos_x = self.pos_x
        self.Layout.pos_y = self.pos_y

        #print("width",self.width)
        #print("height",self.height)
        #print("pos_x",self.pos_x)
        #print("pos_y",self.pos_y)
        #self.isKeyPress('MIDDLEMOUSE')


        #print(draw_menu)
        """
        if self.is_over :
            if self.event_type == "WHEELUPMOUSE" :
                offset = self.WidgetLayout.offset[0]+30
                if offset > 0 :
                    offset = 0

                self.WidgetLayout.offset[0] = offset

            if self.event_type == "WHEELDOWNMOUSE" :
                offset = self.WidgetLayout.offset[0]-30

                self.WidgetLayout.offset[0] = offset
            #WHEELDOWNMOUSE
        """

        if self.is_over and self.context_menu and self.right_mouse.press :
            self.context_menu_close = 0
            self.context_menu_pop = time()

            self.context_menu_show = True
            self.context_menu.pos_x = self.mouse[0] - 15
            self.context_menu.pos_y = self.mouse[1] + 10
            #self.select = True

        #print(self.context_menu.is_over)

        #if time() - self.context_menu_time > 1 :
        #    if   self.context_menu.is_over:
        #        self.context_menu_show = False

        #if time() - self.context_menu_time > 0.2 :
        #    if self.right_mouse.release or self.left_mouse.release :
        #        self.context_menu_show = False

        if time() - self.context_menu_pop > 0.1 :
            if not self.context_menu.is_over :
                self.context_menu_show = False



        for item in [i for i in self.context_menu.items if i.is_pressed] :
            item.run_action()
            item.is_pressed = False
            self.context_menu_show = False
        #print([i for i in self.items if i.is_pressed])
        '''
        if self.context_menu_close and time() - self.context_menu_close > 0.2 :
            #print("close")
            self.context_menu_show = False
            self.context_menu_close = 0
            '''
            #self.area.tag_redraw()
        #if self.event_type in ('ESC',):
        #    self.context_menu_show = False

        #print(self.context_menu.layout.is_over)
        #if not self.context_menu.is_over :
        #    self.context_menu_show = False
        #tmp_down = self.middle_mouse.press



        #print(self.is_over,self.middle_mouse.press)
        #print("value",event.value)
        if self.middle_mouse.press :

            if self.is_over :
                self.is_scrolling = True
                self.scroll_start = self.WidgetLayout.offset[0]
                self.mouse_start = self.mouse_window

        if self.middle_mouse.down and self.is_scrolling :
            self.scroll_x =   self.mouse_window[0] -  self.mouse_start[0]

            #print(self.mouse_start[0],self.middle_mouse.mouse_start[0])
            #print(self.scroll_x)
            offset = self.scroll_start+self.scroll_x
            if offset >0 :
                offset = 0

            self.WidgetLayout.offset = Vector((offset,0))

        if self.middle_mouse.release:
            self.is_scrolling = False


        """
        return
        # Scrollable
        #print(self.middle_mouse.down)
        #self.event_type == key and self.event_value == 'PRESS'
        if self.is_over and self.event.type == 'MIDDLEMOUSE' and self.event.value == 'PRESS' :
            self.is_scrolling = True


        if self.event.type == 'MIDDLEMOUSE' and self.event.value == 'RELEASE' :
            self.is_scrolling = False

        #print(self.event.type)
        return
        #print(self.middle_mouse.mouse_start,self.middle_mouse.mouse_end)$
        print(self.middle_mouse.release)
        if not self.is_over :
            self.middle_mouse.down = False

        if self.is_over and self.middle_mouse.down :
            if not self.is_scrolling :
                self.is_scrolling = True
                self.scroll_start = self.WidgetLayout.offset[0]
                self.mouse_start = self.mouse_window

        #print(self.middle_mouse.down)
        if self.middle_mouse.down and self.is_scrolling :
            self.scroll_x =   self.mouse_window[0] -  self.mouse_start[0]

            #print(self.mouse_start[0],self.middle_mouse.mouse_start[0])
            #print(self.scroll_x)
            offset = self.scroll_start+self.scroll_x
            if offset >0 :
                offset = 0

            self.WidgetLayout.offset = (offset,0)

        if self.middle_mouse.release :
            self.is_scrolling = False



        if self.isKeyPress('MIDDLEMOUSE') :
            print('MIDDLE')
            self.mouse_start = self.mouse
            self.scroll_start = self.WidgetLayout.offset[0]


        if self.is_over and self.middle_mouse :
            print("Start Scrolling")

            if not self.is_scrolling :
                self.mouse_start = self.mouse_window
                self.scroll_start = self.WidgetLayout.offset[0]

            self.is_scrolling = True


        if self.middle_mouse and self.is_scrolling :
            self.scroll_x = self.mouse_window[0] - self.mouse_start[0]
            #print(self.scroll_x)
            offset = self.scroll_start+self.scroll_x
            if offset >0 :
                offset = 0

            self.WidgetLayout.offset = (offset,0)

        if not self.middle_mouse :
            print("Stop Scrolling")

            self.mouse_start = None
            self.scroll_start = None
            self.is_scrolling = False


        if self.isKeyRelease('MIDDLEMOUSE') :
            self.mouse_start = None
            self.scroll_start = None
            """




        """
        if self.is_over  :
            if self.isKeyPress('LEFTMOUSE') and not self.is_pressed :
                self.is_pressed = True


            elif self.isKeyRelease('LEFTMOUSE') :
                self.select = True


        else :
            if self.isKeyPress('LEFTMOUSE') and not self.is_pressed :
                self.select = False

        if self.event_value =='RELEASE':
            self.is_pressed = False
            """

    def draw(self) :

        self.processEvent()

        box = self.bound_box

        #self.Frame.draw()
        #print("width",self.Frame.width)
        #print("height",self.Frame.height)
        #print("pos_x",self.Frame.pos_x)
        #print("pos_y",self.Frame.pos_y)

        """
        pos = box[0]
        pos+= Vector((self.scroll_x,self.scroll_y))

        for item in self.items :
            #self.WidgetLayout.addWidget(item)
            item.pos_x = pos[0]
            item.pos_y = pos[1]
            #item.draw()

            pos += Vector((item.width+self.padding,0))
            """

        self.Layout.draw()

        if self.context_menu_show :
            #print('draw rightmenu')
            self.context_menu.draw()

        #if self.context_menu
        #self.Layout.draw()

        #print(self.Layout)

        #print(self.select)

        #background default color
        #bg_color = (0.5, 0.5, 0.5, 1)
