from .Property import Property
from .Picture import Picture
from ..functions import *

class EnumItem(Property):
    margin = (8,6)

    def __init__(self,data,property,item_id,text=None,icon= None):
        self.state = 'DEFAULT'

        self.data= data
        self.property = property
        self.properties = self.data.bl_rna.properties[self.property]

        items = [i for i in  self.properties.enum_items if i.identifier == item_id]

        #print('##')
        #print("properties",self.properties)

        self.item = None
        if items :
            self.item = items[0]

        self.item_id = self.item.identifier

        self.value = getattr(data,property)

        if text is None:
            self.setText(self.item.name)
        else :
            self.setText(text)

        if icon :
            self.setIcon(icon)


    def setMinimumSize(self,size = None) :
        if size is None :
            if self.icon and not self.text :
                self.minimum_width =  22
                return
            dim = 2*self.margin[0]
            text_dim = self.getTextSize(self.text)
            dim+= text_dim[0]

            if self.icon :
                dim += 10

            self.minimum_width =  dim
        else :
            self.minimum_width =  size



    def processEvent(self) :
        self.isMouseOver()
        self.boundBox()
        self.value = getattr(self.data,self.property)

        if self.is_over  :
            if self.event_type == 'LEFTMOUSE' and self.event_value =='PRESS' and not self.is_pressed:
                setattr(self.data,self.property,self.item.identifier)

                self.is_pressed = True

                self.run_action()

        if self.event_value =='RELEASE':
            self.is_pressed = False

    def draw(self) :

        self.processEvent()

        box = self.bound_box
        glEnable(GL_BLEND)


        #rim
        glColor4f(1, 1, 1, 0.15)

        #print(upper_left,self.rim_offset)
        offset_rim_vector = Vector((self.rim_offset,-self.rim_offset))
        draw_square(*(box+offset_rim_vector))
        #draw_square(upper_left,upper_right,lower_right,lower_left)



        #background default color
        bg_color_top = (0.6, 0.6, 0.6, 1)
        bg_color_bottom = (0.68, 0.68, 0.68, 1)

        #Background mouse hover
        if self.value  == self.item_id :
            bg_color_bottom = self.changeColor(bg_color_bottom,self.dark_offset*4)
            bg_color_top= self.changeColor(bg_color_top,self.dark_offset*4)


        if self.is_over:
            bg_color_top = self.changeColor(bg_color_top,self.bright_offset)
            bg_color_bottom = self.changeColor(bg_color_bottom,self.bright_offset)


        draw_gradient(*box,bg_color_top,bg_color_bottom)


        #contour
        glLineWidth(1)
        draw_square(*box, color =(0.2, 0.2, 0.2, 1), primitive = GL_LINE_STRIP)



        if self.value == self.item_id :
            glColor3f(0.95,0.95,0.95)
        else :
            glColor3f(0.05,0.05,0.05)


        if not self.icon :
            tex_pos = (box[3]+box[2])/2
        else :
            tex_pos = (box[3]+box[2])/2 + Vector((10,0))

        tex_pos += Vector((0,self.margin[1]))
        tex_pos -= Vector((self.getTextSize(self.text)[0]/2,0))
        #tex_pos = box[2]
        # draw op name
        blf.position(self.font_id, *tex_pos, 0)
        blf.size(self.font_id, 13, 72)
        blf.draw(self.font_id, self.text)

        #Draw Icon
        if self.icon :
            #self.icon.pos = box[3]
            #self.icon.draw()

            icon_center = (self.height -self.icon.size[1])/2
            self.icon.pos = box[3] + Vector((icon_center,icon_center))
            self.icon.draw()


        # restore opengl defaults
        glLineWidth(1)
        glDisable(GL_BLEND)
        glColor4f(0.0, 0.0, 0.0, 1.0)
