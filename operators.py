from .functions import *

import subprocess


import bpy
from bgl import *
from mathutils import Vector


def draw_callback_px(self, context):
    if not self.mouse_start : return

    bg_color = (1,1,1,0.15)
    line_color = (1,1,1,0.35)


    self.width = int(abs(self.mouse[0]-self.mouse_start[0]))
    self.height = int(abs(self.mouse[1]-self.mouse_start[1]))

    #ref_size = max((width,height))

    coords = [self.mouse_start,
            (self.mouse[0],self.mouse_start[1]),
            (self.mouse[0],self.mouse_start[1]-self.width),
            (self.mouse_start[0],self.mouse_start[1]-self.width)]

    # Draw square
    glEnable(GL_BLEND)
    glEnable(GL_LINE_STIPPLE)

    '''
    glBegin(GL_POLYGON)
    glColor4f(*bg_color)
    for co in coords : glVertex2f(*co)
    glEnd()
    '''

    glBegin(GL_LINE_LOOP)
    glColor4f(*line_color)
    for co in coords : glVertex2f(*co)
    glEnd()

    glDisable(GL_BLEND)
    glDisable(GL_LINE_STIPPLE)

class Snapshot(bpy.types.Operator):
    """Draw a line with the mouse"""
    bl_idname = "view3d.snapshot"
    bl_label = "Simple Modal View3D Operator"

    mouse = Vector((0,0))
    mouse_start = None

    def modal(self, context, event):
        self.mouse = Vector((event.mouse_region_x,event.mouse_region_y))
        context.area.tag_redraw()

        if event.type in ('RIGHTMOUSE', 'ESC') :
            return {'CANCELLED'}

        #if event.type == 'MOUSEMOVE':
        #    self.mouse_path.append((event.mouse_region_x, event.mouse_region_y))
        if event.type == 'LEFTMOUSE' and event.value == 'PRESS' :
            print('START')
            self.mouse_start = Vector((event.mouse_region_x,event.mouse_region_y))
            self.mouse_window = Vector((event.mouse_x,event.mouse_y))


        elif event.type == 'LEFTMOUSE' and event.value == 'RELEASE' :
            print('END')

            size = self.width-1

            buffer = Buffer(GL_BYTE, size * size * 4)
            glReadBuffer(GL_BACK)

            x = int(self.mouse_window[0]) +1
            y = int(self.mouse_window[1] - size)

            glReadPixels(x,y, size, size, GL_RGBA, GL_UNSIGNED_BYTE, buffer)

            image = bpy.data.images.get('snapshot')
            if not image : image = bpy.data.images.new('snapshot', size, size)
                
            image.scale(size, size)
            image.pixels = [v / 255 for v in buffer]


            bpy.types.SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
            return {'FINISHED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        self._handle = bpy.types.SpaceView3D.draw_handler_add(draw_callback_px, (self,event), 'WINDOW', 'POST_PIXEL')
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}



class AddAsset(bpy.types.Operator):
    """Open the asset loader"""
    bl_idname = "view3d.add_loader"
    bl_label = "Add Asset"

    #def draw(self, context):


    def execute(self, context):
        blender_path = bpy.app.binary_path
        template = join(dirname(__file__),"preview_generator.blend")

        batch = join(dirname(__file__),"bg_add_assets.py")

        clipboard = context.window_manager.clipboard

        store = env_vars["STORE"]
        lib = get_lib_dir()

        files = []
        for i,file in enumerate(eval(clipboard)) :
            files.append(file)
            if i%3 == 0 :

                subprocess.call([blender_path,template,'-p','4096','0','0','0','--python',batch,'--',str(files),store,lib])
                files = []
        return {'FINISHED'}


    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self,width=150)
