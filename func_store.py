import bpy,json,os
from .utils import *



def has_keyframe(action,data_path,index,frame) :
    if not action :
        return

    fc= action.fcurves.find(data_path,index)

    if not fc :
        return

    keyframes = {int(p.co[0]):p.co[1] for p in fc.keyframe_points}

    if frame in keyframes:
        return round(keyframes[frame],6)


def channel_values(ob,data_path,frame,default_values,options = {'ONLY_KEYED'}):
    channel = None
    action = get_action(ob)

    if isinstance(default_values,(int,float)) :
        keyframe = has_keyframe(action,data_path,0,frame)

        if keyframe != None :
            return keyframe

        elif "ONLY_KEYED" not in options :
            prop_value = ob.path_resolve(data_path)
            if round(prop_value,3) != default_values :
                return prop_value

        elif "ALL" in options :
            return ob.path_resolve(data_path)

        return

    channel = {}
    for i,v in enumerate(default_values) :
        keyframe = has_keyframe(action,data_path,i,frame)

        if keyframe != None :
            channel[i] = keyframe

        elif "ONLY_KEYED" not in options and round(ob.path_resolve(data_path)[i],3) != v :
            channel[i] = ob.path_resolve(data_path)[i]

        elif "ALL" in options :
            channel[i] = ob.path_resolve(data_path)[i]

    return channel




def mirror_transform(transforms):
    from copy import deepcopy

    transforms = deepcopy(transforms)

    mirror_channels = {
        'location' : (0,),
        'rotation_euler' : (1,2),
        'rotation_quaternion' : (2,3),
        'rotation_axis_angle' : (2,3) }

    for channel,values in transforms.items() :
        if channel not in mirror_channels : continue

        indexes = mirror_channels[channel]
        for index,value in values.items() :
            if index not in indexes : continue

            transforms[channel][index] = -value

    return transforms



def store_pose_3d(ob= None,frame = None,options = {'ONLY_KEYED'},bones = []) :
    # options in {'ALL','ONLY_KEYED','NONE_ZERO','MIRROR'}
    scene = bpy.context.scene
    pose_data = {}

    if not ob :
        ob = bpy.context.object
        if not ob :
            print('No active object')
            return

    action = get_action(ob)
    if not action and 'ONLY_KEYED' in options :
        print('The active object as no action')
        return

    if not frame : frame = scene.frame_current

    if not bones : bones = ob.pose.bones

    for b in bones :
        bone_data = {}
        bone_transforms = {}
        bone_props = {}

        bone_name = b.name

        ######    CUSTOM PROP     #########
        for prop in [k for k,v in b.items() if k not in ['_RNA_UI'] and isinstance(v,(int,float))]:
            data_path = 'pose.bones["%s"]["%s"]'%(b.name,prop)
            prop_value = channel_values(ob,data_path,frame,0,options)
            if prop_value != None : bone_props[prop] = prop_value

        if bone_props :  bone_data['props'] = bone_props

        #####    LOCATION     ##########
        data_path = 'pose.bones["%s"].location'%b.name
        location = channel_values(ob,data_path,frame,(0,0,0),options)

        if location : bone_transforms['location'] = location

        ######   ROTATION  #######
        if b.rotation_mode == 'QUATERNION' :
            channel = 'rotation_quaternion'
            default_value = (1,0,0,0)
        elif b.rotation_mode == 'AXIS_ANGLE' :
            channel = 'rotation_axis_angle'
            default_value = (0,1,0,0)
        else :
            channel = 'rotation_euler'
            default_value = (0,0,0)

        data_path = 'pose.bones["%s"].%s'%(b.name,channel)
        rotation = channel_values(ob,data_path,frame,default_value,options)

        if rotation : bone_transforms[channel] = rotation

        #####  SCALE
        data_path = 'pose.bones["%s"].scale'%b.name
        scale = channel_values(ob,data_path,frame,(1,1,1),options)

        if scale : bone_transforms['scale'] = scale

        if bone_transforms : bone_data['transforms'] = bone_transforms

        if bone_transforms or bone_props :
            pose_data[bone_name] = bone_data



    #mirror
    if 'MIRROR' in options :
        print('Mirror in Options \n')
        sides = list(map(get_name_side,pose_data))
        side = max(('LEFT','RIGHT'), key = lambda x : sides.count(x))

        mirror_pose_data = {}
        for bone_name,bone_data in pose_data.items() :
            mirror_bone = find_mirror(bone_name)
            mirror_bone_data = {}

            if mirror_bone :
                mirror_bone_data['transforms'] = mirror_transform(bone_data['transforms'])

            if bone_data.get('props') :
                if not mirror_bone : mirror_bone = bone_name

                mirror_bone_props = {}
                for k,v in bone_data['props'].items() :
                    mirror_prop = find_mirror(k)
                    if not mirror_prop :
                        mirror_bone_props[k] = v

                    elif mirror_prop and get_name_side(mirror_prop) == side :
                        mirror_bone_props[mirror_prop] = v

                if mirror_bone_props : mirror_bone_data['props'] = mirror_bone_props

            if mirror_bone_data : mirror_pose_data[mirror_bone] = mirror_bone_data

        pose_data.update(mirror_pose_data)



    print(pose_data)
    return pose_data







def store_anim_3d(start,end,only_selected,bezier,mirror) :
    Fcurves={}

    ob = bpy.context.scene.objects.active
    actionFcurves = ob.animation_data.action.fcurves

    if only_selected :
        fcurves =[]
        for fc in actionFcurves :
            if fc.is_valid :
                bone = ob.pose.bones.get(fc.data_path.split('"')[1])
                if bone and bone.bone.select == True:
                    fcurves.append(fc)

        #fcurves = [fc for fc in actionFcurves if fc.is_valid and ob.pose.bones.get(fc.data_path.split('"')[1]).bone.select==True]
    else :
        fcurves = [fc for fc in actionFcurves]

    for fc in fcurves :

        boneId = fc.data_path.split('"')[1]

        mirror_boneId = find_mirror(boneId)
        #print(boneId,mirror_boneId)

        if fc.data_path.endswith(']') :
            prop = fc.data_path.replace('pose.bones["%s"]'%(boneId),"")
        else :
            prop = fc.data_path.replace('pose.bones["%s"].'%(boneId),"")

        mirror_prop = find_mirror(prop)
        #print(prop,mirror_prop)

        keyframe_attribute = []
        for keyframe in fc.keyframe_points :
            if keyframe.co[0] in range(start,end+1) :

                if not Fcurves.get(boneId) :
                    Fcurves[boneId]={}

                if not Fcurves[boneId].get(prop) :
                    Fcurves[boneId][prop] = {}

                if not Fcurves[boneId][prop].get(fc.array_index) :
                    Fcurves[boneId][prop][fc.array_index] = []


                keyframe_attribute = [
                    keyframe.interpolation,
                    [keyframe.co[0]-start,round(keyframe.co[1],8)],
                    keyframe.type,
                    ]

                if bezier == True :
                    keyframe_attribute+=[

                    [round(keyframe.handle_left[0],5),round(keyframe.handle_left[1],5)],
                    keyframe.handle_left_type,
                    [round(keyframe.handle_right[0],5),round(keyframe.handle_right[1],5)],
                    keyframe.handle_right_type,
                    keyframe.easing,
                    round(keyframe.back,5),
                    round(keyframe.amplitude,5),
                    round(keyframe.period,5),
                    ]

            if keyframe_attribute :
                Fcurves[boneId][prop][fc.array_index].append(keyframe_attribute)

                if mirror :
                    mirror_attribute = keyframe_attribute.copy()

                    if mirror_boneId and not mirror_prop :
                        if not Fcurves.get(mirror_boneId) :
                            Fcurves[mirror_boneId]={}

                        if not Fcurves[mirror_boneId].get(prop) :
                            Fcurves[mirror_boneId][prop] = {}

                        if not Fcurves[mirror_boneId][prop].get(fc.array_index) :
                            Fcurves[mirror_boneId][prop][fc.array_index] = []


                        Fcurves[mirror_boneId][prop][fc.array_index].append(mirror_key(prop,fc.array_index,mirror_attribute,bezier))

                    elif mirror_prop and not mirror_boneId:
                        if not Fcurves[boneId].get(mirror_prop) :
                            Fcurves[boneId][mirror_prop] = {}

                        if not Fcurves[boneId][mirror_prop].get(fc.array_index) :
                            Fcurves[boneId][mirror_prop][fc.array_index] = []

                        Fcurves[boneId][mirror_prop][fc.array_index].append(mirror_attribute)

                    elif mirror_boneId and mirror_prop:
                        if not Fcurves.get(mirror_boneId) :
                            Fcurves[mirror_boneId]={}

                        if not Fcurves[mirror_boneId].get(mirror_prop) :
                            Fcurves[mirror_boneId][mirror_prop] = {}

                        if not Fcurves[mirror_boneId][mirror_prop].get(fc.array_index) :
                            Fcurves[mirror_boneId][mirror_prop][fc.array_index] = []

                        Fcurves[mirror_boneId][mirror_prop][fc.array_index].append(mirror_key(prop,fc.array_index,mirror_attribute,bezier))

    return Fcurves
